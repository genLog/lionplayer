package main;

import common.Common;
import common.Language;
import common.Preference;
import controller.Util;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import view.Dialogs;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
        try {
            Locale.setDefault(Locale.ENGLISH);

            Locale appLocale = (Preference.getApplicationLanguage() == Language.ENGLISH ? Locale.ENGLISH : Locale.FRENCH);
            ResourceBundle resourceBundle = ResourceBundle.getBundle("ApplicationResources", appLocale);
            Dialogs.setResourceBundle(resourceBundle);

            setUserAgentStylesheet(STYLESHEET_CASPIAN);
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(resourceBundle);
            loader.setLocation(getClass().getResource(Common.MAIN_WINDOW_FXML));
            Parent root = loader.load();

            Scene scene = new Scene(root, 1200, 700);
            primaryStage.getIcons().add(new Image(getClass().getResource(Common.ICON_APPLICATION).toString()));
            primaryStage.setTitle(Common.APP_NAME);
            primaryStage.setScene(scene);
            primaryStage.setOnHidden(e -> Platform.exit());
            primaryStage.show();

            try {
                Util.openPlaylist(Preference.getLastPlaylist());
            } catch (FileNotFoundException e) {}
            Preference.getLibrary();
            Preference.getLstPlaylistsPaths();
        } catch (IOException ex) {
            ex.printStackTrace();
            System.exit(-1);
        }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
