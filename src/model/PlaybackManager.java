package model;

import common.Common;
import common.Preference;
import interfaces.Observable;
import interfaces.Observation;
import interfaces.Observer;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.util.Duration;

import java.util.AbstractList;
import java.util.ArrayList;

/**
 * Description.
 *
 * @author Marc-André Dion
 * @since 2015-11-17
 */
public class PlaybackManager implements Observable {
    // Nombre de répétions par défaut
    private static final int DEFAULT_REPEAT_COUNT = 1;
    // The actual media player
    private MediaPlayer mediaPlayer;
    // Observateurs
    private AbstractList<Observer> observers;

    // Etat du lecteur
    private Observation state;

    public PlaybackManager() {
        observers = new ArrayList<>();
        notifyObservers(Observation.NOTHING_TO_PLAY);
    }

    public PlaybackManager(Observer observer) {
        observers = new ArrayList<>();
        addObserver(observer);
        state = Observation.NOTHING_TO_PLAY;
    }

    /**
     * Charge une chanson.
     *
     * @param media      le media a lire
     * @param repeatSong si la chanson doit se répéter
     * @param autoPlay   si le lecteur doir démarrer la lecture dès que l'état est prêt
     */
    void loadMedia(Media media, boolean repeatSong, boolean autoPlay) {
        if (mediaPlayer != null) {
            mediaPlayer.dispose();
        }
        mediaPlayer = new MediaPlayer(media);
        resetVolume();
        setStateListeners();
        setRepeat(repeatSong);
        mediaPlayer.setAutoPlay(autoPlay);
    }

    /**
     * Pour faire jouer en boucle
     *
     * @param repeatSong vrai si on veux boucler
     */
    void setRepeat(boolean repeatSong) {
        int cycles = repeatSong ? MediaPlayer.INDEFINITE : DEFAULT_REPEAT_COUNT;
        if (state != Observation.NOTHING_TO_PLAY) {
            mediaPlayer.setCycleCount(cycles);
        }
    }

    /**
     * Indique les methodes a executé selon le changement activé
     */
    private void setStateListeners() {
        mediaPlayer.setOnError(() -> notifyObservers(Observation.PLAYER_ERROR));
        mediaPlayer.setOnReady(() -> notifyObservers(Observation.PLAYER_READY));
        mediaPlayer.setOnPlaying(() -> notifyObservers(Observation.PLAYER_STARTED));
        mediaPlayer.setOnPaused(() -> notifyObservers(Observation.PLAYER_PAUSED));
        mediaPlayer.setOnStopped(() -> notifyObservers(Observation.PLAYER_STOPPED));
        mediaPlayer.setOnEndOfMedia(() -> notifyObservers(Observation.PLAYBACK_REACHED_END_OF_MEDIA));
        mediaPlayer.setOnRepeat(() -> notifyObservers(Observation.PLAYER_STARTED));
    }

    /**
     * Renvoit le dernier état duquel les observateurs ont été notifiés.
     *
     * @return le dernier état du lecteur
     */
    Observation getLastState() {
        return state;
    }

    /**
     * Commute entre la joute et la pause
     */
    void togglePlayback() {
        if (state != Observation.NOTHING_TO_PLAY) {
            if (state == Observation.PLAYER_STARTED) {
                mediaPlayer.pause();
            } else {
                mediaPlayer.play();
            }
        }
    }

    /**
     * Arrete la lecture et reinitialise le temps écoulé
     */
    void stopPlayback() {
        if (state != Observation.NOTHING_TO_PLAY) {
            mediaPlayer.stop();
        }
    }

    /**
     * Le niveau du volume
     *
     * @return le volume du player entre 0.0 et 1.0 inclusivement
     */
    double getVolume() {
        double volume = Preference.getLastSetVolume();
        if (state != Observation.NOTHING_TO_PLAY) {
            volume = mediaPlayer.getVolume();
        }
        return volume;
    }

    /**
     * Ajuste le niveau du volume
     *
     * @param volume Volume entre 0 (muet) et 1 (plein volume).
     */
    void setVolume(double volume) {
        if (volume <= Common.VOLUME_MAX && volume >= Common.VOLUME_MIN && state != Observation.NOTHING_TO_PLAY) {
            mediaPlayer.setVolume(volume);
            Preference.putLastSetVolume(volume);
        }
    }

    /**
     * Set le volume selon les derniere preference utilisateur.
     */
    private void resetVolume() {
        mediaPlayer.setVolume(Preference.getLastSetVolume());
    }

    /**
     * Le temps écoulé depuis le début de la chanson.
     *
     * @return un double du nombre de secondes écoulé
     */
    double getMediaElapsedTime() {
        return mediaPlayer.getCurrentTime().toSeconds();
    }

    /**
     * Le pourcentage écoulé de la chanson.
     *
     * @return le pourcentage écoulé de la chanson entre 0.0 et 1.0
     */
    double getMediaElapsedPercentage() {
        if(state == Observation.NOTHING_TO_PLAY){
            throw new IllegalStateException("No media loaded");
        }
        return getMediaElapsedTime() / mediaPlayer.getCycleDuration().toSeconds();
    }

    /**
     * Ajuste la tête de lecture à la position voulue.
     *
     * @param position la position voulue entre 0.0 et 1.0
     */
    void seekMediaPosition(double position) {
        mediaLoadedCheck();
        if(position >= 0 && position <= 1.0){
            mediaPlayer.seek(Duration.seconds(mediaPlayer.getCycleDuration().toSeconds() * position));
        }
        else{
            throw new IllegalArgumentException("Expected in range (0.0, 1.0) : actual : " + position);
        }
    }

    /**
     * Décharge le media du lecteur peut importe son état.
     */
    void unloadMedia() {
        mediaLoadedCheck();
        notifyObservers(Observation.NOTHING_TO_PLAY);
        mediaPlayer.dispose();
    }

    private void mediaLoadedCheck(){
        if(state == Observation.NOTHING_TO_PLAY){
            throw new IllegalStateException("No media loaded");
        }
    }

    public MediaPlayer getMediaPlayer() {
        return mediaPlayer;
    }

    @Override
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    @Override
    public void notifyObservers(Observation argument) {
        state = argument;
        for (Observer observer : observers) {
            observer.update(argument);
        }
    }
}
