package model;

import controller.PlaylistController;
import controller.Util;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.media.Media;
import model.exceptions.SongException;
import org.apache.commons.io.FilenameUtils;
import org.jaudiotagger.audio.AudioFile;
import org.jaudiotagger.audio.AudioFileIO;
import org.jaudiotagger.audio.exceptions.*;
import org.jaudiotagger.tag.FieldDataInvalidException;
import org.jaudiotagger.tag.FieldKey;
import org.jaudiotagger.tag.Tag;
import org.jaudiotagger.tag.TagException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Path;
import java.time.LocalTime;

/**
 * Classe de la piste, permet a l'utilisateur d'avoir une piste.
 *
 * @author Steve Mbiele
 * @since 9/9/2015.
 */
public class Song extends MediaItem implements Serializable {
    private static Logger log = LoggerFactory.getLogger(PlaylistController.class);

    private String title;
    private String artist;
    private String album;
    private String genre;
    private String year;
    private int length;
    private File file;
    private Image image;
    private String timeString;
    private Tag tag;
    private AudioFile audioFile;

    /**
     * Constructeur par défaut pour la sérialisation.
     */
    public Song() {
        super(null);
    }

    public Song(Path path) throws SongException {
        super(path);
        this.file = path.toFile();
        fillInformations();
    }

    public String getAlbum() {
        return album;
    }

    public String getArtist() {
        return artist;
    }


    public String getTitle() {
        return title;
    }

    public File getFile() {
        return file;
    }

    public String getYear() {
        return year;
    }

    public Image getImage() {
        return image;
    }

    public String getTimeString() {
        return timeString;
    }

    public int getLength() {
        return length;
    }

    public void saveMetadata() throws FieldDataInvalidException, CannotWriteException {
        tag.setField(FieldKey.TITLE, title);
        tag.setField(FieldKey.ARTIST, artist);
        tag.setField(FieldKey.ALBUM, album);
        tag.setField(FieldKey.YEAR, year);
        tag.setField(FieldKey.GENRE, genre);
        audioFile.setTag(tag);
        audioFile.commit();
        Player.getInstance().triggerSongEditedEvent();
    }

    private void fillInformations() throws SongException {
        try {
            audioFile = AudioFileIO.read(file);
        } catch (TagException
                | ReadOnlyFileException
                | CannotReadException
                | InvalidAudioFrameException
                | IOException e) {
            throw new SongException(file, e);
        }

        tag = audioFile.getTagOrCreateDefault();

        title = tag.getFirst(FieldKey.TITLE);
        artist = tag.getFirst(FieldKey.ARTIST);
        album = tag.getFirst(FieldKey.ALBUM);
        year = tag.getFirst(FieldKey.YEAR);
        genre = tag.getFirst(FieldKey.GENRE);
        length = audioFile.getAudioHeader().getTrackLength();
        timeString = parseSeconds();

        completeMissingInformations();

        if (tag.getFirstArtwork() != null) {
            try {
                image = convertToImage(tag.getFirstArtwork().getBinaryData());
            } catch (IOException e) {
                log.error("Erreur de io lors du chargement de l'image du tag", e);
            }
        } else {
            try {
                image = Util.getLastFmImages(this.artist);
            } catch (IOException e) {
                log.error("Erreur de io lors du chargement de l'image de last fm", e);

            }
        }
    }

    /**
     * Simple seconds to HH:MM:SS with zeros trimmed out
     */
    private String parseSeconds() {
        String dureeTxt = LocalTime.ofSecondOfDay(length).toString();
        while (dureeTxt.length() > 4 && (dureeTxt.startsWith("0") || dureeTxt.startsWith(":"))) {
            dureeTxt = dureeTxt.substring(1);
        }
        return dureeTxt;
    }

    /**
     * pour les chansons qui n'ont pas toutes les informations, ceci remplace les inconnus par Unknown
     */
    private void completeMissingInformations() {
        if (album == null || album.equals("")) {
            album = "Unknown";
        }
        if (artist == null || artist.equals("")) {
            artist = "Unknown";
        }
        if (title == null || title.equals("")) {
            title = FilenameUtils.removeExtension(file.getName());
        }
    }

    /**
     * converti un byte array en image
     *
     * @param bytes le byte array a convertir
     * @return retourne une image
     * @throws IOException
     */
    private static Image convertToImage(byte[] bytes) throws IOException {
        Image image = null;
        if (bytes != null) {
            ByteArrayInputStream tabByte = new ByteArrayInputStream(bytes);
            BufferedImage img = ImageIO.read(tabByte);
            image = SwingFXUtils.toFXImage(img, null);
        }
        return image;
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Song)
            return this.file.getPath().equals(((Song) other).file.getPath());

        return false;
    }

    public String getPath() {
        return file.toURI().toString();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
