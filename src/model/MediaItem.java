package model;

import javafx.scene.media.Media;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.tag.FieldDataInvalidException;

import java.io.File;
import java.nio.file.Path;

/**
 * Created by Dan on 2015-12-07.
 */
public abstract class MediaItem {
    Media media;

    public MediaItem(Path path) {
        media = new Media(path.toUri().toString());
    }

    public Media getMedia() {
        return media;
    }

    abstract public String getTitle();

    abstract public String getArtist();

    abstract public File getFile();

    abstract public String getYear();

    abstract public String getTimeString();

    abstract public int getLength();

    abstract public void saveMetadata() throws FieldDataInvalidException, CannotWriteException;

    @Override
    public abstract boolean equals(Object other);

    abstract public String getPath();

    abstract void setTitle(String title);

    abstract void setArtist(String artist);

    abstract void setYear(String year);
}
