package model;

import javafx.scene.media.Media;
import org.apache.commons.io.FilenameUtils;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.tag.FieldDataInvalidException;

import java.io.File;
import java.io.Serializable;
import java.nio.file.Path;
import java.time.LocalTime;

/**
 * Classe de la piste, permet a l'utilisateur d'avoir une piste.
 *
 * @author Steve Mbiele
 * @since 9/9/2015.
 */
public class Video extends MediaItem implements Serializable {
    private String title;
    private String artist;
    private String year;
    private int length;
    private File file;
    private String timeString;

    /**
     * Constructeur par défaut pour la sérialisation.
     */
    public Video() {
        super(null);
    }

    public Video(Path path) {
        super(path);
        this.file = path.toFile();
        fillInformations();
    }

    public String getArtist() {
        return artist;
    }

    public String getTitle() {
        return title;
    }

    public File getFile() {
        return file;
    }

    public String getYear() {
        return year;
    }

    public String getTimeString() {
        return timeString;
    }

    public int getLength() {
        return length;
    }

    @Override
    public void saveMetadata() throws FieldDataInvalidException, CannotWriteException {

    }

    private void fillInformations() {
        Media temp = new Media(file.toURI().toString());
        artist = (String) temp.getMetadata().get("artist");
        title = (String) temp.getMetadata().get("title");
        completeMissingInformations();
    }

    /**
     * Simple seconds to HH:MM:SS with zeros trimmed out
     */
    private String parseSeconds() {
        String dureeTxt = LocalTime.ofSecondOfDay(length).toString();
        while (dureeTxt.length() > 4 && (dureeTxt.startsWith("0") || dureeTxt.startsWith(":"))) {
            dureeTxt = dureeTxt.substring(1);
        }
        return dureeTxt;
    }

    /**
     * pour les chansons qui n'ont pas toutes les informations, ceci remplace les inconnus par Unknown
     */
    private void completeMissingInformations() {
        if (artist == null || artist.equals("")) {
            artist = "Unknown";
        }
        if (title == null || title.equals("")) {
            title = FilenameUtils.removeExtension(file.getName());
        }
    }

    @Override
    public boolean equals(Object other) {
        if (other instanceof Video)
            return this.file.getPath().equals(((Video) other).file.getPath());

        return false;
    }

    public String getPath() {
        return file.toURI().toString();
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public void setLength(int length) {
        this.length = length;
        if (length != 0) {
            parseSeconds();
        }
    }
}
