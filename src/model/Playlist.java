package model;


import common.Common;
import common.Preference;
import controller.PlaylistController;
import controller.Util;
import model.exceptions.SongException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Classe de la liste de lecture, permet a l'utilisateur d'avoir une liste de musique.
 *
 * @author Steve Mbiele
 * @since 9/9/2015.
 */
public class Playlist implements Serializable {
    private static Logger log = LoggerFactory.getLogger(PlaylistController.class);

    // Nom de la liste de lecture
    private String playlistName;
    // liste de chansons
    private List<MediaItem> mediaItems;
    // date de creation de la playliste
    private Date creationDate;

    /**
     * Constructeur avec le nom est une liste de musique
     *
     * @param name  le nom de la playlist
     * @param mediaItems la liste de chanson � ajouter dans la playlist
     */
    public Playlist(String name, List<MediaItem> mediaItems) {
        this.playlistName = name;
        this.mediaItems = mediaItems;
        creationDate = new Date();
    }

    /**
     * Constructeur de la playliste avec le nom
     * @param name le nom de la playliste
     */
    public Playlist(String name) {
        this.playlistName = name;
        this.mediaItems = new ArrayList<>();
        creationDate = new Date();
    }

    /**
     * Contrusteur de la playliste avec un fichier sauvegarder
     * @param playlist
     */
    public Playlist(File playlist) {
        this.mediaItems = new ArrayList<>();
        load(playlist.getAbsolutePath());
    }

    public String getCreationDate() {
        return Common.DATE_FORMAT.format(creationDate);
    }

    public String getPlaylistName() {
        return playlistName;
    }

    public void setPlaylistName(String name) {
        this.playlistName = name;
    }

    public List<MediaItem> getMediaItems() {
        return mediaItems;
    }

    public int getPlaylistLength() {
        return mediaItems.size();
    }

    /**
     * Ajoute une chanson à la playliste
     *
     * @param mediaItem la chanson à ajouter
     */
    public void addMedia(MediaItem mediaItem) {
        if (!exists(mediaItem)) {
            this.mediaItems.add(mediaItem);
        }
    }

    /**
     * Ajoute une liste de chanson à la playliste
     * @param mediaItems
     */
    public void addAllMedia(List<MediaItem> mediaItems){
        for(MediaItem mediaItem : mediaItems){
            addMedia(mediaItem);
        }
    }

    /**
     * enleve une musique de la playliste
     *
     * @param mediaItem la chanson � enlever
     */
    public void removeMedia(MediaItem mediaItem) {
        if (exists(mediaItem)) {
            mediaItems.remove(mediaItem);
        }
    }

    /**
     * Verifie si la chanson recu en parametre existe dans la playliste
     *
     * @param mediaItem la chanson � verifier
     * @return vrai ou faux
     */
    private boolean exists(MediaItem mediaItem) {
        return mediaItems.contains(mediaItem);
    }

    public MediaItem mediaAt(int songId) {
        return mediaItems.get(songId);
    }

    /**
     * Retourne la position d'une chanson, en partant de 0.
     * @param mediaItem Chanson cherchée.
     * @return Position de cette chanson.
     */
    public int indexOf(MediaItem mediaItem){
        return mediaItems.indexOf(mediaItem);
    }

    public void moveAfter(MediaItem toMove, MediaItem afterThis){
        mediaItems.remove(toMove);
        mediaItems.add(mediaItems.indexOf(afterThis) + 1, toMove);
    }

    public void moveBefore(MediaItem toMove, MediaItem beforeThis){
        mediaItems.remove(toMove);
        mediaItems.add(mediaItems.indexOf(beforeThis), toMove);
    }

    /**
     * Sauvegarde al playliste
     * @param fileToSave le fichier de sauvegarde
     */
    public void save(File fileToSave) {
        List<String> playlist = new ArrayList<>();
        playlist.add(getPlaylistName());
        playlist.add(Common.DATE_FORMAT.format(creationDate));
        for (MediaItem mediaItem : mediaItems) {
            playlist.add(mediaItem.getFile().getAbsolutePath());
        }

        try (OutputStream file = new FileOutputStream(fileToSave);
             OutputStream buffer = new BufferedOutputStream(file);
             ObjectOutput output = new ObjectOutputStream(buffer)) {
            output.writeObject(playlist);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        Util.addPlaylistPath(fileToSave.getAbsolutePath());
        Preference.putLastPlaylistPath(fileToSave.getAbsolutePath());
    }

    private void load(String playlistPath) {
        try (InputStream file = new FileInputStream(playlistPath);
             InputStream buffer = new BufferedInputStream(file);
             ObjectInput input = new ObjectInputStream(buffer)) {
             List<String> playlist = (List<String>) input.readObject();

            playlistName = playlist.get(0);
            creationDate = Common.DATE_FORMAT.parse(playlist.get(1));
            for (String path : playlist.subList(2, playlist.size())) {
                try {
                    addMedia(new Song(Paths.get(path)));
                } catch(SongException ex) {}
            }
            Preference.putLastPlaylistPath(playlistPath);
        } catch (ClassNotFoundException | IOException | ParseException ex) {
            log.error("Erreur de chargement de playlist", ex);
        }
    }
}
