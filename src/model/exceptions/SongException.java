package model.exceptions;

import java.io.File;

/**
 * Exception survenue lors de la lecture des informations d'une chanson.
 */
public class SongException extends Exception {
    public SongException(File songFile, Exception causedBy){
        super("Erreur lors de la lecture du fichier «" + songFile.getAbsolutePath() + "».", causedBy);
    }
}
