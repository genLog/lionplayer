package model;

import common.Preference;
import controller.PlaylistController;
import javafx.beans.property.ReadOnlyListWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import model.exceptions.SongException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Contient la liste de toutes les chansons dans sa totalité.
 */
public class Library {
    private static Logger log = LoggerFactory.getLogger(PlaylistController.class);
    private static Library instance = new Library();

    public static Library getInstance(){
        return instance;
    }

    /**
     * Liste complète de toutes les chansons.
     */
    private ObservableList<Song> songs = FXCollections.observableArrayList();

    /**
     * Liste complète de toutes les chansons.
     */
    private ObservableList<Video> videos = FXCollections.observableArrayList();

    /**
     * Constructeur privé par défaut.
     */
    private Library(){}

    /**
     * Ajoute toutes les chansons d'une liste à la bibliothèque.
     * @param mediaItems Liste de laquelle ajouter les chansons.
     */
    public void addAllMedia(List<MediaItem> mediaItems){
        for (MediaItem mediaItem : mediaItems) {
            if (mediaItem instanceof Video) {
                if (isInLibrary((Video)mediaItem)) {
                    videos.add((Video)mediaItem);
                }
            } else if (!isInLibrary((Song)mediaItem)) {
                songs.add((Song)mediaItem);
            }
        }
    }

    /**
     * Ajoute toutes les videos d'une liste à la bibliothèque
     * @param selectedFiles Liste de laquelle ajouter les videos
     */
    public void addAllVideos(List<Video> selectedFiles) {
        for (Video video : selectedFiles) {
            if (!isInLibrary(video)) {
                videos.add(video);
            }
        }
    }

    /**
     * Checks if a song is already in the library.
     *
     * @param newSong song to be checked
     * @return true if the song is in the library, false otherwise
     */
    private boolean isInLibrary(Song newSong) {
        for (Song song : songs) {
            if (song.equals(newSong))
                return true;
        }

        return false;
    }

    private boolean isInLibrary(Video newVideo) {
        for (Video video : videos) {
            if (video.equals(newVideo))
                return true;
        }

        return false;
    }

    /**
     * Retourne la liste des chansons en lecture seule.
     * @return liste des chansons de la bibliothèque
     */
    public ObservableList<Song> getSongList(){
        return new ReadOnlyListWrapper<>(songs);
    }

    public List<MediaItem> getSongsWithSameArtist(Song compareSong){
        ArrayList<MediaItem> list = new ArrayList<>();

        for(Song song : songs){
            if(song.getArtist().equals(compareSong.getArtist()))
                list.add(song);
        }

        return list;
    }

    public List<MediaItem> getSongsWithSameAlbum(Song compareSong){
        ArrayList<MediaItem> list = new ArrayList<>();

        for(Song song : songs){
            if(song.getAlbum().equals(compareSong.getAlbum()))
                list.add(song);
        }

        return list;
    }

    public void save(File fileToSave) {
        List<String> playlist = new ArrayList<>();
        for (Song song : songs) {
            playlist.add(song.getFile().getAbsolutePath().toString());
        }

        try (OutputStream file = new FileOutputStream(fileToSave);
             OutputStream buffer = new BufferedOutputStream(file);
             ObjectOutput output = new ObjectOutputStream(buffer)) {
            output.writeObject(playlist);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        Preference.putLibraryPath(fileToSave.getAbsolutePath());
    }

    public void load(String playlistPath) {
        try (InputStream file = new FileInputStream(playlistPath);
             InputStream buffer = new BufferedInputStream(file);
             ObjectInput input = new ObjectInputStream(buffer)) {
            List<String> playlist = (List<String>) input.readObject();
            ArrayList <MediaItem> lstMedia = new ArrayList<>();
            for (String path : playlist) {
                try {
                    lstMedia.add(new Song(Paths.get(path)));
                } catch(SongException ex) {}
            }
            addAllMedia(lstMedia);
            Preference.putLibraryPath(playlistPath);
        } catch (ClassNotFoundException | IOException ex) {
            log.error("Erreur de chargement de playlist", ex);
        }
    }
}
