package model;

import javafx.scene.media.EqualizerBand;

/**
 * Données de l'égalisateur.
 */
public class EqualizerData {
    public static final double[] BAND_FREQUENCIES
            = {32, 64, 125, 250, 500, 1000, 2000, 4000, 8000, 16000};

    private static EqualizerData instance = new EqualizerData();
    
    public static EqualizerData getInstance() {
        return instance;
    }

    /** Valeurs choisies par l'utilisateur. */
    private final double[] currentGains = new double[BAND_FREQUENCIES.length];
    
    private EqualizerData() {}

    public void setGain(int index, double value){
        currentGains[index] = value;
    }

    public double getGain(int index){
        return currentGains[index];
    }

    public void updateAllBandsOnCurrentEqualizer(){
        int i = 0;
        for(EqualizerBand band : Player.getInstance().getAudioEqualizer().getBands()) {
            band.setGain(currentGains[i]);
            i++;
        }
    }
}
