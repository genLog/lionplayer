package model;

import interfaces.Observable;
import interfaces.Observation;
import interfaces.Observer;
import common.Common;
import common.RepeatState;
import javafx.scene.image.Image;
import javafx.scene.media.AudioEqualizer;
import javafx.scene.media.MediaPlayer;

import java.util.ArrayList;
import java.util.Random;

/**
 * Principale classe du modèle. Gère les listes de lecture et permet de démarrer
 * la lecture d'une piste.
 */
public class Player implements Observable, Observer {
    // Instance du noyau
    private static Player ourInstance = new Player();
    // Gestionnaire de lecture
    private final PlaybackManager playbackManager;
    // Observateurs.
    private final ArrayList<Observer> lstObserver;
    // Liste de lecture actuellement chargée
    private Playlist playlist;
    //Index de la dernière piste dans la liste de lecture.
    private int idLastSong;
    // Si le lecteur est arrêté, prochaine piste à jouer. Sinon, piste en cours.
    private MediaItem currentMedia;
    /* Si nul, les pistes seront jouées selon l'ordre croissant de leurs numéros.
     * Sinon, elles suivront l'ordre de ce tableau. */
    private int[] songOrder;
    /* Si {@link #songOrder} est nul, numéro de la piste. Sinon, correspond à l'indice
     * dans le tableau {@link #songOrder}. */
    private int playingSongIndex;
    // État de la répétition : activée, activée pour la playlist, activée pour la piste.
    private RepeatState repeatState;

    // Constructeur privé par défaut.
    private Player() {
        lstObserver = new ArrayList<>();
        repeatState = RepeatState.NO_REPEAT;
        playbackManager = new PlaybackManager(this);
    }

    void reset() {
        if(getState() != Observation.NOTHING_TO_PLAY){
            playbackManager.unloadMedia();
        }
        lstObserver.clear();
        playlist = null;
        idLastSong = 0;
        currentMedia = null;
        songOrder = null;
        playingSongIndex = 0;
        repeatState = RepeatState.NO_REPEAT;

    }

    public static Player getInstance() {
        return ourInstance;
    }

    /**
     * Prépare le lecteur à jouer la chanson d'identifiant {@link #playingSongIndex}.
     */
    private void load(boolean autoplay) {
        currentMedia = playlist.mediaAt(isShuffleOn() ? songOrder[playingSongIndex] : playingSongIndex);
        boolean repeat = repeatState == RepeatState.REPEAT_ONE;
        playbackManager.loadMedia(currentMedia.getMedia(), repeat, autoplay);
    }

    /**
     * Alterne entre l'état «en train de jouer» et l'état «en pause».
     */
    public void togglePlay() {
        if (playbackManager.getLastState() == Observation.NOTHING_TO_PLAY) {
            load(true);
        } else {
            playbackManager.togglePlayback();
        }
    }

    /**
     * Arrête complètement la lecture. Re-mélange les pistes
     * si l'aléatoire est activé.
     */
    public void stop() {
        if(getState() != Observation.NOTHING_TO_PLAY){
            playbackManager.stopPlayback();
        }
    }

    public void next() {
        next(false);
    }

    /**
     * Va à la piste suivante.
     */
    private void next(boolean isEndofMediaReached) {
        stop();

        if (playingSongIndex < idLastSong) {
            playingSongIndex++;
            load(true);
        } else {
            playingSongIndex = Common.SMALLEST_INDEX;

            // Réarrange les pistes (si lecture aléatoire activée)
            if (isShuffleOn()) {
                turnOnShuffle(false);
            }

            boolean autoplay = repeatState == RepeatState.REPEAT_ALL || !isEndofMediaReached;
            load(autoplay);
        }
    }

    /**
     * Va à la piste precedente.
     */
    public void previous() {
        double elapsedTime = playbackManager.getMediaElapsedTime();
        stop();

        if (elapsedTime > Common.MAX_TIME_BEFORE_PREVIOUS) {
            load(true);
        } else if (playingSongIndex > Common.SMALLEST_INDEX) {
            playingSongIndex--;
            load(true);
        } else {
            // Réarrange les pistes (si lecture aléatoire activée)
            if (isShuffleOn()) {
                turnOnShuffle();
            }

            playingSongIndex = repeatState == RepeatState.REPEAT_ALL ? idLastSong : Common.SMALLEST_INDEX;
            load(repeatState == RepeatState.REPEAT_ALL);
        }
    }

    public Observation getState() {
        return playbackManager.getLastState();
    }


    /**
     * Méthode à appeler si des modifications ont eu lieu sur la liste de lecture.
     */
    public void updatePlaylist() {
        if (playlist.getPlaylistLength() == 0) {
            playbackManager.unloadMedia();
            notifyObservers(Observation.SONG_REMOVED_OR_ADDED);
        } else if (idLastSong != playlist.getPlaylistLength() - 1) {
            idLastSong = playlist.getPlaylistLength() - 1;
            notifyObservers(Observation.SONG_REMOVED_OR_ADDED);
        }
        if (playlist.getPlaylistLength() != 0 && (currentMedia.equals((playlist.mediaAt(playingSongIndex))))) {
            load(false);
        }
    }

    void triggerSongEditedEvent() {
        notifyObservers(Observation.SONG_METADATA_UPDATED);
    }

    /**
     * Retourne la chanson qui est chargé par le lecteur
     * @return la chanson qui est chargé
     */
    public MediaItem getCurrentMedia() {
        return currentMedia;
    }

    /**
     * Définit l'identifiant numérique de la prochaine chanson à jouer (commence à 0). Il s'agit de sa position
     * dans la liste de lecture.
     *
     * @param playingSongId Identifiant de la chanson.
     */
    public void setPlayingSongId(int playingSongId) {
        stop();

        if (isShuffleOn()) {
            for (int i = 0; i < songOrder.length; i++) {
                if (songOrder[i] == playingSongId) {
                    playingSongIndex = i;
                    break;
                }
            }
        } else
            playingSongIndex = playingSongId;
        load(true);
    }

    public RepeatState getRepeatState() {
        return repeatState;
    }

    public void setRepeatState(RepeatState repeatState) {
        this.repeatState = repeatState;
        playbackManager.setRepeat(repeatState == RepeatState.REPEAT_ONE);
    }

    /**
     * Active la lecture aléatoire. Si elle est déjà activée, réinitialise l'ordre des pistes.
     *
     * @param stayOnCurrentTrack Si vrai, s'assure que la piste en cours est la même après l'appel à cette méthode.
     */
    public void turnOnShuffle(boolean stayOnCurrentTrack) {
        int lastPlayingId;
        Random r = new Random();

        if (!stayOnCurrentTrack)
            lastPlayingId = r.nextInt(songOrder.length);
        else if (isShuffleOn())
            lastPlayingId = songOrder[playingSongIndex];
        else
            lastPlayingId = playingSongIndex;

        songOrder = new int[playlist.getPlaylistLength()];
        ArrayList<Integer> alreadyInserted = new ArrayList<>();

        songOrder[0] = lastPlayingId;
        alreadyInserted.add(lastPlayingId);

        for (int i = 1; i < songOrder.length; i++) {
            int nextId = r.nextInt(songOrder.length);

            if (!alreadyInserted.contains(nextId)) {
                songOrder[i] = nextId;
                alreadyInserted.add(nextId);
            } else {
                i--;
            }
        }
    }

    /**
     * Active la lecture aléatoire. Si elle est déjà activée, réinitialise l'ordre des pistes. S'assure que la piste
     * en cours est la même après l'appel à cette méthode.
     */
    public void turnOnShuffle() {
        turnOnShuffle(true);
    }

    /**
     * Désactive la lecture aléatoire. S'assure que la piste en cours est la même après l'appel à cette méthode.
     */
    public void turnOffShuffle() {
        playingSongIndex = songOrder[playingSongIndex];
        songOrder = null;
    }

    public boolean isShuffleOn() {
        return songOrder != null;
    }

    /**
     * Si la musique est en train de jouer, renvoie la fraction de la chanson qui a
     * été jouée, sous forme d'un nombre de 0 à 1. Sinon, renvoie 0.
     *
     * @return Nombre de 0 à 1.
     */
    public double getCurrentSongPosition() {
        if (getState() == Observation.PLAYER_STARTED || getState() == Observation.PLAYER_PAUSED)
            return playbackManager.getMediaElapsedPercentage();
        else
            return 0;
    }

    /**
     * Déplace le lecteur à la position spécifiée dans la chanson.
     *
     * @param position Nombre de 0 à 1.
     */
    public void seekPosition(double position) {
        playbackManager.seekMediaPosition(position);
    }

    public String getSongTitle() {
        String songTitle;
        songTitle = currentMedia.getTitle();
        if (songTitle == null) {
            songTitle = currentMedia.getFile().getName().toString();
        }
        return songTitle;
    }

    public String getSongArtist() {
        return currentMedia.getArtist();
    }

    public String getSongAlbum() {
        if (currentMedia instanceof Song){
            return ((Song)currentMedia).getAlbum();
        }
        return "";
    }

    public Playlist getPlaylist() {
        return playlist;
    }

    /**
     * Définit la liste de lecture, et la prochaine chanson à jouer sera
     * la première de la liste, à moins d'appeler {@link #setPlayingSongId(int)}.
     *
     * @param playlist Liste de lecture.
     */
    public void setPlaylist(Playlist playlist) {
        stop();
        this.playlist = playlist;
        playingSongIndex = 0;
        idLastSong = playlist.getPlaylistLength() - 1;
        load(false);
        notifyObservers(Observation.PLAYLIST_SET);
    }

    /**
     * Crée une liste de lecture avec une seule chanson, et la définit.
     * @param song Chanson à ajouter.
     */
    public void setSingleSongPlaylist(Song song){
        ArrayList<MediaItem> list = new ArrayList<>();
        list.add(song);
        setPlaylist(new Playlist(song.getTitle(), list));
    }

    public void setPlaylistFromArtistOf(Song song){
        setPlaylist(new Playlist(song.getArtist(), Library.getInstance().getSongsWithSameArtist(song)));
    }

    public void setPlaylistFromAlbumOf(Song song){
        setPlaylist(new Playlist(song.getAlbum(), Library.getInstance().getSongsWithSameAlbum(song)));
    }

    /**
     * La valeur du volume si le player n'est pas Status.UNKNOWN, sinon renvoit valeur la derniere valeur du volume
     *
     * @return le volume du player entre 0.0 et 1.0 inclusivement
     */
    public double getVolume() {
        return playbackManager.getVolume();
    }

    /**
     * Ajuste le volume si le lecteur n'est pas en Status.UNKNOWN.
     *
     * @param volume Volume entre 0 (muet) et 1 (plein volume).
     */
    public void setVolume(double volume) {
        playbackManager.setVolume(volume);
    }

    /**
     * La valeur du volume sur 100 si le player n'est pas Status.UNKNOWN, sinon renvoit valeur la derniere valeur du volume
     *
     * @return le volume du player entre 0 et 100 inclusivement
     */
    public int getVolumePourcentage() {
        Double volume = getVolume() * 100;
        return volume.intValue();
    }

    public Image getSongCoverArt() {
        if (currentMedia instanceof Song){
            return ((Song)currentMedia).getImage();
        }
        return null;
    }

    public MediaPlayer getMediaPlayer() {
        return playbackManager.getMediaPlayer();
    }

    public AudioEqualizer getAudioEqualizer(){
        return playbackManager.getMediaPlayer().getAudioEqualizer();
    }

    @Override
    public void addObserver(Observer observer) {
        lstObserver.add(observer);
    }

    @Override
    public void notifyObservers(Observation argument) {
        for (Observer observer : lstObserver) {
            if (observer != null) {
                observer.update(argument);
            }
        }
    }

    @Override
    public void update(Observation argument) {
        switch (argument) {
            case PLAYLIST_SET:
                break;
            case NOTHING_TO_PLAY:
                notifyObservers(Observation.NOTHING_TO_PLAY);
                break;
            case PLAYER_PAUSED:
                notifyObservers(Observation.PLAYER_PAUSED);
                break;
            case PLAYER_STOPPED:
                notifyObservers(Observation.PLAYER_PAUSED);
                notifyObservers(Observation.PLAYER_STOPPED);
                break;
            case PLAYER_STARTED:
                notifyObservers(Observation.PLAYER_STARTED);
                break;
            case SONG_LOADED:
                break;
            case SONG_REMOVED_OR_ADDED:
                break;
            case SONG_METADATA_UPDATED:
                break;
            case PLAYER_READY:
                notifyObservers(Observation.PLAYER_READY);
                notifyObservers(Observation.SONG_LOADED);
                break;
            case PLAYER_ERROR:
                break;
            case PLAYBACK_REACHED_END_OF_MEDIA:
                if (repeatState != RepeatState.REPEAT_ONE) {
                    next(true);
                }
                notifyObservers(Observation.PLAYER_STARTED);
                break;
        }
    }
}
