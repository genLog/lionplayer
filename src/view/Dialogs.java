package view;

import com.sun.deploy.util.FXLoader;
import common.Language;
import common.Preference;
import controller.EqualizerController;
import controller.Util;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import model.Player;
import model.Playlist;
import model.Song;
import model.Video;
import model.exceptions.SongException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Permet l'affichage de boîtes de dialogue.
 */
public class Dialogs {
    private static Dialogs instance;

    public static void setResourceBundle(ResourceBundle bundle){
        instance = new Dialogs(bundle);
    }

    public static Dialogs getInstance(){
        return instance;
    }

    private ResourceBundle bundle;

    private Dialogs(ResourceBundle bundle){
        this.bundle = bundle;
    }

    /**
     * Creates and displays the About dialog box
     */
    public void showAboutDialog(){
        Alert alert = new Alert(AlertType.INFORMATION);
        alert.setTitle(bundle.getString("about-title"));
        alert.setHeaderText(bundle.getString("version"));
        alert.setContentText(bundle.getString("authors"));

        alert.showAndWait();
    }

    public void showPreferencesDialog(){
        showModal("../view/preference-dialog.fxml", "preferences-title", (stage, root, loader) -> {
            ComboBox comboBox = (ComboBox) root.lookup("#languagePicker");
            String french = bundle.getString("french");
            String english = bundle.getString("english");

            comboBox.getItems().setAll(french, english);

            ((Button) root.lookup("#btnValider")).setOnAction(event -> {
                Language newLanguage = comboBox.getValue().equals(french) ? Language.FRENCH : Language.ENGLISH;
                Preference.putApplicationLanguage(newLanguage);
                stage.close();
            });

            ((Button) root.lookup("#btnCancel")).setOnAction(event -> {
                stage.close();
            });
        });
    }

    public void showEqualizerDialog(){
        showModal("../view/equalizer-dialog.fxml", "equalizer-title", (stage, root, loader) -> {
            EqualizerController controller = loader.getController();
            controller.setStage(stage);
            Player.getInstance().addObserver(controller);
        });
    }

    private void showModal(String resourcePath, String windowTitleKey, ModalInitializer modalInitializer){
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setResources(bundle);
            loader.setLocation(getClass().getResource(resourcePath));
            Parent root = loader.load();

            Stage stage = new Stage();
            stage.setTitle(bundle.getString(windowTitleKey));
            stage.setScene(new Scene(root));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.setResizable(false);
            stage.show();

            modalInitializer.initWindowComponents(stage, root, loader);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates and displays the About dialog box
     * @param message
     */
    public void showErrorDialog(String message){
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(bundle.getString("error-title"));
        alert.setHeaderText(bundle.getString("error-header"));
        alert.setContentText(message);

        alert.showAndWait();
    }

    /**
     * Opens a FileChooser to select one or more songs.
     * @return Chosen song list, or null if no songs have been selected.
     */
    public List openFileChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(bundle.getString("open-file-title"));
        fileChooser.setInitialDirectory(Preference.getLastOpenedDirectory());
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter(bundle.getString("all-files"), "*.mp3", "*.wav"),
                new FileChooser.ExtensionFilter(bundle.getString("mp3"), "*.mp3"),
                new FileChooser.ExtensionFilter(bundle.getString("wav"), "*.wav"),
                new FileChooser.ExtensionFilter(bundle.getString("mp4"), "*.mp4")
        );

        List<File> list = fileChooser.showOpenMultipleDialog(null);

        if (list != null) {
            ArrayList returnedSongList = new ArrayList<Song>();
            ArrayList returnedVideoList = new ArrayList<Video>();
            for (File file : list) {
                if (!file.getName().substring(file.getName().indexOf(".") + 1, file.getName().length()).equals("mp4")){
                    try {
                        Song song = new Song(file.toPath());
                        returnedSongList.add(song);
                    } catch(SongException ex) {}
                } else {
                    Video video = new Video(file.toPath());
                    returnedVideoList.add(video);
                }
            }

            Preference.putLastOpenedDirectory(list.get(0).getParentFile());

            if (returnedSongList.size() != 0) {
                return returnedSongList;
            }
            else {
                return returnedVideoList;
            }
        }

        return null;
    }

    /**
     * Opens a FileChooser to select a playlist
     */
    public void openPlaylistChooser() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(bundle.getString("open-playlist-title"));
        fileChooser.setInitialDirectory(Preference.getLastOpenedDirectory());
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(bundle.getString("playlist"), "*.lion"));
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            Util.openPlaylist(new Playlist(file));
            Preference.putLastOpenedDirectory(file.getParentFile());
        }
    }

    /**
     * Opens a FileChooser to save a playlist
     */
    public void openSavePlaylistDialog() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(bundle.getString("save-playlist-title"));
        fileChooser.setInitialDirectory(Preference.getLastOpenedDirectory());
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(bundle.getString("playlist"), "*.lion"));
        File file = fileChooser.showSaveDialog(null);
        if (file != null) {
            Player.getInstance().getPlaylist().save(file);
            Preference.putLastOpenedDirectory(file.getParentFile());
        }
    }

    private interface ModalInitializer {
        void initWindowComponents(Stage stage, Parent root, FXMLLoader loader);
    }
}
