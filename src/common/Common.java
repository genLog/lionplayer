package common;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;

/**
 * constante utiliser poar toute l'application
 *
 * @author Steve Mbiele
 * @since 9/8/2015.
 */
public final class Common {
    /**
     * Constructeur privé par défaut. Empêche l'instanciation.
     */
    private Common() throws IllegalAccessException {
        throw new IllegalAccessException("Impossible d'instancier cette classe.");
    }

    public static final String APP_NAME = "Lion Player";

    public static final String MAIN_WINDOW_FXML = "../view/main-window.fxml";
    public static final String EDITOR_WINDOW_FXML = "../view/tag-editor.fxml";
    public static final Path PATH_TEST_URL1 = Paths.get("BitRush2.mp3");
    public static final Path PATH_TEST_URL2 = Paths.get("LoginScreenIntro.mp3");
    public static final long MAX_TIME_BEFORE_PREVIOUS = 3;
    public static final int SMALLEST_INDEX = 0;
    public static final String SEPARATOR = " - ";
    public static final double VOLUME_MAX = 1;
    public static final double VOLUME_MIN = 0;
    public static final String OS = (System.getProperty("os.name")).toUpperCase();
    public static final String LIBRARY_SAVE = "lionLib.lion";
    public static final String PATHS_SAVE = "LionPlaylistsPaths.lion";
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    public static final String ICON_APPLICATION = "../resources/icons/lion.png";
    public static final String PLAYING_ICON = "../resources/icons/playing.png";
    public static final String API_KEY ="b25b959554ed76058ac220b7b2e0a026";
    public static final String API_SECRET = "88436207871bd6d40ac64fdfa319770a";
    public static final String ACCOUNT_NAME = "Fastboyx";
}
