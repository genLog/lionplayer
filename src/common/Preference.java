package common;

import controller.Util;
import model.Library;
import model.Playlist;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.prefs.*;

/**
 * Saves and load user preferences. This class could be adapted to also access system wide preference if need be.
 *
 * @author Marc-André
 * @since 29/09/2015.
 */
public class Preference {
    // Keys
    private final static String KEY_LAST_OPENED_FOLDER = "last_opened_folder";
    private final static String KEY_LAST_SET_VOLUME = "last_player_volume";
    private final static String KEY_LAST_PLAYLIST = "last_playlist";
    private final static String KEY_LANGUAGE_ID = "lang_id";
    private final static String KEY_LIBRABRY = "library_onclosing";
    private final static String KEY_LST_PLAYLIST_PATHS = "playlists_paths";

    // Default values
    private final static String LAST_OPENED_FOLDER_DEFAULT = System.getProperty("user.home");
    private final static double LAST_SET_VOLUME_DEFAULT = 1;
    private final static String LAST_PLAYLIST_DEFAULT = "";
    private final static String LIBRARY_DEFAULT = "";
    private final static String PLAYLIST_PATHS_DEFAULT = "";

    // User preferences
    private static final Preferences userPref = Preferences.userNodeForPackage(Preference.class);

    /**
     * Private constructor throws Exception, this is a static class.
     */
    private Preference(){
        throw new UnsupportedOperationException("Instanciation not allowed");
    }

    /**
     * Get the last opened directory or the user default if none found, all in file format.
     * @return The last opened directory in file format.
     */
    public static File getLastOpenedDirectory(){
        String path = userPref.get(KEY_LAST_OPENED_FOLDER,LAST_OPENED_FOLDER_DEFAULT);
        File lastDir = new File(path);
        if (lastDir.exists() && lastDir.isDirectory()){
            return new File(path);
        }
        else{
            return new File(LAST_OPENED_FOLDER_DEFAULT);
        }
    }

    /**
     * Saves the received file's path as the last opened directory
     * @param file last opened directory
     */
    public static void putLastOpenedDirectory(File file){
        if(file != null && file.exists() && file.isDirectory()){
            userPref.put(KEY_LAST_OPENED_FOLDER,file.getAbsolutePath());
        }
    }

    /**
     * Get the last set volume
     * @return the last set volume between 0 and 1
     */
    public static double getLastSetVolume(){
        return userPref.getDouble(KEY_LAST_SET_VOLUME,LAST_SET_VOLUME_DEFAULT);
    }

    /**
     * Get the last set volume
     * @return the last set volume between 0 and 100
     */
    public static int getLastSetVolumePercentage(){
        Double volume = getLastSetVolume();
        volume *= 100;
        return volume.intValue();
    }

    /**
     * Saves the received double as the new volume provided it is valid
     * @param volume the new volume
     */
    public static void putLastSetVolume(double volume){
        if(volume >= Common.VOLUME_MIN && volume <= Common.VOLUME_MAX){
            userPref.putDouble(KEY_LAST_SET_VOLUME, volume);
        }
    }

    /**
     * Get the last set playlist.
     * @return the last saved/opened playlist
     * @throws FileNotFoundException if the file doesn't exist
     */
    public static Playlist getLastPlaylist() throws FileNotFoundException{
        File playlist = new File(userPref.get(KEY_LAST_PLAYLIST,LAST_PLAYLIST_DEFAULT));
        if(playlist.exists() && playlist.isFile()){
            return new Playlist(playlist);
        }
        putLastPlaylistPath(LAST_PLAYLIST_DEFAULT);
        throw new FileNotFoundException(playlist.getAbsolutePath());
    }

    /**
     * Saves the last playlist path
     * @param path path to last playlist
     */
    public static void putLastPlaylistPath(String path){
        userPref.put(KEY_LAST_PLAYLIST, path);
    }

    public static void getLibrary() throws FileNotFoundException{
        File playlist = new File(userPref.get(KEY_LIBRABRY,LIBRARY_DEFAULT));
        if(playlist.exists() && playlist.isFile()){
            Library.getInstance().load(playlist.getAbsolutePath());
        }
    }

    public static void putLibraryPath(String path){
        userPref.put(KEY_LIBRABRY, path);
    }

    public static void putApplicationLanguage(Language locale){
        userPref.putInt(KEY_LANGUAGE_ID, locale.getId());
    }

    public static Language getApplicationLanguage(){
        return Language.getLanguageFromId(userPref.getInt(KEY_LANGUAGE_ID, 1));
    }

    /**
     * Clears preferences
     * @return true if successful
     */
    public static boolean clear(){
        try {
            userPref.clear();
            return true;
        } catch (BackingStoreException e) {
            return false;
        }
    }

    public static void putLstPlaylistsPaths(String absolutePath) {
        userPref.put(KEY_LST_PLAYLIST_PATHS, absolutePath);
    }

    public static void getLstPlaylistsPaths() throws FileNotFoundException{
        File playlist = new File(userPref.get(KEY_LST_PLAYLIST_PATHS,PLAYLIST_PATHS_DEFAULT));
        if(playlist.exists() && playlist.isFile()){
            Util.loadPaths(playlist.getAbsolutePath());
        }
    }
}
