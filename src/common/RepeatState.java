package common;

/**
 * Les différents état de répétition.
 *
 * @author Steve Mbiele
 * @since 9/16/2015.
 */
public enum RepeatState {
    REPEAT_ALL,
    REPEAT_ONE,
    NO_REPEAT
}
