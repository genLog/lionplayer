package common;

/**
 * Created by maxim on 15-11-11.
 */
public enum Language {
    FRENCH(0),
    ENGLISH(1);

    public static Language getLanguageFromId(int id){
        if(id == 0)
            return FRENCH;

        if(id == 1)
            return ENGLISH;

        throw new RuntimeException("Aucune langue de numéro " + id);
    }

    private final int id;

    Language(int id){
        this.id = id;
    }

    public int getId(){
        return id;
    }
}
