package controller;

import common.Common;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import model.Song;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.tag.FieldDataInvalidException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import view.Dialogs;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Controlleur de l'objet tag-editor
 *
 * @author Mark
 * @since 10/11/2015
 */
public class TagEditorController implements Initializable {
    private static Logger log = LoggerFactory.getLogger(PlaylistController.class);
    private Song song;
    private Stage stage;
    private ResourceBundle bundle;

    @FXML
    private Button btnValider;
    @FXML
    private Button btnCancel;
    @FXML
    private TextField txtFilePath;
    @FXML
    private TextField txtTitle;
    @FXML
    private TextField txtArtist;
    @FXML
    private TextField txtAlbum;
    @FXML
    private TextField txtYear;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bundle = resources;
        btnValider.setOnAction(event -> onClickHandler(btnValider));
        btnCancel.setOnAction(event -> onClickHandler(btnCancel));
    }

    /**
     * Handles clicks on buttons in the window
     * @param btn
     */
    private void onClickHandler(Button btn) {
        if (btn == btnValider) {
            song.setTitle(txtTitle.getText());
            song.setArtist(txtArtist.getText());
            song.setAlbum(txtAlbum.getText());
            song.setYear(txtYear.getText());
            try {
                song.saveMetadata();
                stage.close();
            } catch (FieldDataInvalidException e) {
                log.info("Champs invalide dans le tag", e);
                Dialogs.getInstance().showErrorDialog(bundle.getString("error-savemetadata-input"));
            } catch (CannotWriteException e) {
                log.info("Impossible d'écrire les changements", e);
                Dialogs.getInstance().showErrorDialog(bundle.getString("error-savemetadata-save"));
            }
        } else if(btn == btnCancel) {
            stage.close();
        }
    }

    /**
     * Receives a song and load the metadata in the fields
     *
     * @param selectedSong
     */
    public void loadMetadata(Song selectedSong) {
        song = selectedSong;
        txtFilePath.setText(song.getPath());
        txtTitle.setText(song.getTitle());
        txtArtist.setText(song.getArtist());
        txtAlbum.setText(song.getAlbum());
        txtYear.setText(song.getYear());
    }

    /**
     * Set the stage instance of this window to allow the controller to close it when done
     * @param stage
     */
    public void setStage(Stage stage) {
        this.stage = stage;
        stage.getIcons().add(new javafx.scene.image.Image(getClass().getResource(Common.ICON_APPLICATION).toString()));

    }
}
