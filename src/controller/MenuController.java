package controller;

import common.Common;
import interfaces.Observation;
import interfaces.Observer;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import model.*;
import view.Dialogs;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Contrôleur de la barre de menus du haut.
 */
public class MenuController implements Initializable, Observer {
    @FXML
    private MenuItem menuPlayPause;
    @FXML
    private MenuItem menuStop;
    @FXML
    private MenuItem menuOpenFile;
    @FXML
    private MenuItem menuOpenPlaylist;
    @FXML
    private MenuItem menuSavePlaylist;
    @FXML
    private MenuItem menuClose;
    @FXML
    private MenuItem menuPreferences;
    @FXML
    private MenuItem menuEqualizer;
    @FXML
    private MenuItem menuAbout;

    private Dialogs dialogs = Dialogs.getInstance();

    private ResourceBundle bundle;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bundle = resources;

        menuClose.setOnAction(event -> Platform.exit());
        menuPlayPause.setOnAction(event -> Player.getInstance().togglePlay());
        menuStop.setOnAction(event -> Player.getInstance().stop());
        menuOpenFile.setOnAction(event -> doOpenFileMenuAction());
        menuOpenPlaylist.setOnAction(event -> dialogs.openPlaylistChooser());
        menuSavePlaylist.setOnAction(event -> dialogs.openSavePlaylistDialog());
        menuPreferences.setOnAction(event -> dialogs.showPreferencesDialog());
        menuEqualizer.setOnAction(event -> dialogs.showEqualizerDialog());
        menuAbout.setOnAction(event -> dialogs.showAboutDialog());

        Player.getInstance().addObserver(this);
    }

    /**
     * Ouvre la boîte de dialogue de sélection de fichier(s) et ajoute le(s) fichier(s) sélectionné(s) à
     * la biliothèque et dans une nouvelle liste de lecture.
     */
    private void doOpenFileMenuAction(){
        List selectedFiles = Dialogs.getInstance().openFileChooser();

        if(selectedFiles != null) {

            String path = Util.getOs();
            if (selectedFiles.get(0) instanceof Song) {
                Library.getInstance().addAllMedia(selectedFiles);
            } else if (selectedFiles.get(0) instanceof Video) {
                Library.getInstance().addAllVideos(selectedFiles);
            }
            File file = new File(path + File.separator + Common.LIBRARY_SAVE);
            if (!file.getParentFile().exists() || !file.getParentFile().isDirectory()){
                file.getParentFile().mkdirs();
            }

            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (file != null) {
                Library.getInstance().save(file);
            }

            Player.getInstance().setPlaylist(new Playlist(bundle.getString("new-playlist"), selectedFiles));
        }
    }



    @Override
    public void update(Observation argument) {
        switch (argument){
            case PLAYLIST_SET:
                menuSavePlaylist.setDisable(false);
                break;
            case NOTHING_TO_PLAY:
                menuSavePlaylist.setDisable(true);
                break;
            case PLAYER_STARTED:
                menuPlayPause.setText(bundle.getString("pause"));
                break;
            case PLAYER_STOPPED:
            case PLAYER_PAUSED:
                menuPlayPause.setText(bundle.getString("play"));
        }
    }
}
