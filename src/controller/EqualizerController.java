package controller;

import interfaces.Observation;
import interfaces.Observer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.media.EqualizerBand;
import javafx.stage.Stage;
import model.EqualizerData;
import model.Player;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Contrôleur pour la boîte de dialogue d'égalisateur.
 */
public class EqualizerController implements Initializable, Observer {
    @FXML
    private HBox sliderContainer;
    @FXML
    private Button btnValider;

    private Stage stage;

    private EqualizerData equalizerData = EqualizerData.getInstance();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        int i = 0;

        for(double frequency : EqualizerData.BAND_FREQUENCIES){
            addNewBandSlider(frequency, i);
            i++;
        }

        btnValider.setOnAction(event -> stage.close());
    }

    private void addNewBandSlider(final double frequency, final int index){
        VBox vbox = new VBox();
        Label frequencyLabel = new Label(Math.round(frequency) + " Hz");
        Slider slider = new Slider();
        HBox.setHgrow(vbox, Priority.ALWAYS);

        slider.setOrientation(Orientation.VERTICAL);
        slider.setMin(EqualizerBand.MIN_GAIN);
        slider.setMax(EqualizerBand.MAX_GAIN);
        slider.setValue(equalizerData.getGain(index));
        VBox.setVgrow(slider, Priority.ALWAYS);

        vbox.setAlignment(Pos.CENTER);
        vbox.getChildren().addAll(frequencyLabel, slider);
        sliderContainer.getChildren().add(vbox);

        slider.valueProperty().addListener((observable, oldValue, newValue) -> {
            equalizerData.setGain(index, newValue.doubleValue());
            if(Player.getInstance().getState() == Observation.PLAYER_STARTED)
                equalizerData.updateAllBandsOnCurrentEqualizer();
        });
    }

    /**
     * Définit le {@link Stage} afin de permettre de fermer la fenêtre.
     * @param stage
     */
    public void setStage(Stage stage){
        this.stage = stage;
    }

    @Override
    public void update(Observation argument) {
        switch (argument) {
            case SONG_LOADED:
            case PLAYER_STARTED:
                equalizerData.updateAllBandsOnCurrentEqualizer();
                break;
        }
    }
}
