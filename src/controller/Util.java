package controller;

import common.Common;
import common.Preference;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import model.Library;
import model.Player;
import model.Playlist;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * Méthodes utilitaires.
 */
public final class Util {
    private static Logger log = LoggerFactory.getLogger(PlaylistController.class);
    private static ArrayList<String> lstPathPlayList = new ArrayList<>();
    private static ObservableList<Playlist> lstPlaylists = FXCollections.observableArrayList();
    /**
     * Constructeur privé.
     */
    private Util() throws IllegalAccessException {
        throw new IllegalAccessException("Cette classe n'est pas instanciable.");
    }

    /**
     * Opens the playlist received in parameter
     * @param playlist playlist object
     */
    public static void openPlaylist(Playlist playlist) {
        Player.getInstance().setPlaylist(playlist);
        Library.getInstance().addAllMedia(playlist.getMediaItems());
    }

    public static void addPlaylistPath(String path){
        lstPathPlayList.add(path);
        lstPlaylists.add(new Playlist(new File(path)));
        saveLstPathPlaylist();
    }

    private static void saveLstPathPlaylist() {
        String path = getOs() + File.separator + Common.PATHS_SAVE;
        File fileToSave = new File(path);
        if (!fileToSave.getParentFile().exists() || !fileToSave.getParentFile().isDirectory()){
            fileToSave.getParentFile().mkdirs();
        }
        try {
            fileToSave.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }


        try (OutputStream file = new FileOutputStream(fileToSave);
             OutputStream buffer = new BufferedOutputStream(file);
             ObjectOutput output = new ObjectOutputStream(buffer)) {
            output.writeObject(lstPathPlayList);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        Preference.putLstPlaylistsPaths(fileToSave.getAbsolutePath());
    }

    public static String getOs() {
        String path;
        if (Common.OS.contains("WIN")){
            path = System.getenv("AppData") + File.separator+ "Lionplayer";
        }else{
            path = System.getenv("user.home") + File.separator + "Lionplayer";
        }
        return path;
    }

    public static void loadPaths(String absolutePath) {
        try (InputStream file = new FileInputStream(absolutePath);
             InputStream buffer = new BufferedInputStream(file);
             ObjectInput input = new ObjectInputStream(buffer)) {
            List<String> lstPaths = (List<String>) input.readObject();
            for (String path : lstPaths) {
                File playlist = new File(path);
                if (playlist.exists()){
                    lstPathPlayList.add(path);
                    lstPlaylists.add(new Playlist(playlist));
                }
            }
        } catch (ClassNotFoundException | IOException ex) {
            log.error("Erreur de chargement de playlist", ex);
        }
    }

    public static ObservableList<Playlist> getPlaylists() {
        return lstPlaylists;
    }

    public static Image getLastFmImages(String artistName) throws IOException {
        String apiUrl = "http://ws.audioscrobbler.com/2.0/?method=artist.getinfo&artist="
                + URLEncoder.encode(artistName)
                + "&api_key="
                + Common.API_KEY;

        Document doc = Jsoup.connect(apiUrl).timeout(20000).get();
        Elements images = doc.select("image");

        Element image = images.get(3);
        Connection.Response resultImageResponse = Jsoup.connect(image.getElementsByAttributeValue("size", "extralarge").text())
                .ignoreContentType(true).execute();

        ByteArrayInputStream tabByte = new ByteArrayInputStream(resultImageResponse.bodyAsBytes());
        BufferedImage img = ImageIO.read(tabByte);
        return SwingFXUtils.toFXImage(img, null);
    }
}
