package controller;

import interfaces.Observation;
import interfaces.Observer;
import common.RepeatState;
import javafx.animation.Interpolator;
import javafx.animation.ScaleTransition;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import model.Player;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by maxim on 15-10-06.
 */
public class ToolbarController implements Initializable, Observer {
    private static final Duration BUTTON_ANIMATION_DURATION = Duration.millis(200);
    @FXML
    private Button btnPlay;
    @FXML
    private Button btnStop;
    @FXML
    private Button btnNext;
    @FXML
    private Button btnPrevious;
    @FXML
    private Button btnRepeat;
    @FXML
    private Button btnShuffle;
    @FXML
    private Label songTitle;
    @FXML
    private Label songArtist;
    @FXML
    private Label songAlbum;
    @FXML
    private Slider volumeSlider;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        btnPlay.setOnAction(event -> Player.getInstance().togglePlay());
        btnStop.setOnAction(event -> Player.getInstance().stop());
        btnNext.setOnAction(event -> next());
        btnPrevious.setOnAction(event -> previous());
        btnRepeat.setOnAction(event -> changeRepeatState());
        btnShuffle.setOnAction(event -> changeShuffleState());

        volumeSlider.adjustValue(Player.getInstance().getVolumePourcentage());
        volumeSlider.valueProperty().addListener((observable, oldValue, newValue)
                -> ajusterVolume(newValue.doubleValue()));

        for(Button button : new Button[]{btnPlay, btnStop, btnNext, btnPrevious, btnRepeat, btnShuffle}){
            createButtonMouseOverHandler(button);
            createButtonMouseOutHandler(button);
            button.setScaleX(0.9);
            button.setScaleY(0.9);
        }

        Player.getInstance().addObserver(this);
    }

    private void createButtonMouseOverHandler(Button button){
        button.setOnMouseEntered(event -> {
            ScaleTransition transition = new ScaleTransition(BUTTON_ANIMATION_DURATION);
            transition.setToX(1.1);
            transition.setToY(1.1);
            transition.setNode(button);
            transition.setInterpolator(Interpolator.EASE_BOTH);
            transition.play();
        });
    }

    private void createButtonMouseOutHandler(Button button){
        button.setOnMouseExited(event -> {
            ScaleTransition transition = new ScaleTransition(BUTTON_ANIMATION_DURATION);
            transition.setToX(0.9);
            transition.setToY(0.9);
            transition.setNode(button);
            transition.setInterpolator(Interpolator.EASE_BOTH);
            transition.play();
        });
    }

    /**
     * Modifie l'interface pour afficher des boutons/menus «pause».
     */
    private void setInterfacePlaying(){
        if(!btnPlay.getStyleClass().contains("pause-button"))
            btnPlay.getStyleClass().add("pause-button");
    }

    /**
     * Modifie l'interface pour afficher des boutons/menus «pause».
     */
    private void setInterfaceNotPlaying(){
        if(btnPlay.getStyleClass().contains("pause-button"))
            btnPlay.getStyleClass().remove("pause-button");
    }

    private void ajusterVolume(double newValue) {
        Player.getInstance().setVolume(newValue / 100);
    }

    private void previous() {
        Player.getInstance().previous();
    }

    private void next() {
        Player.getInstance().next();
    }

    /**
     * (Dés)active la lecture aléatoire.
     */
    private void changeShuffleState(){
        if(Player.getInstance().isShuffleOn()){
            btnShuffle.getStyleClass().remove("selected");
            Player.getInstance().turnOffShuffle();
        } else {
            btnShuffle.getStyleClass().add("selected");
            Player.getInstance().turnOnShuffle();
        }
    }

    /**
     * Selon le RepeatState actuel, le change à un autre.
     */
    private void changeRepeatState(){
        RepeatState current = Player.getInstance().getRepeatState();

        if(current == RepeatState.NO_REPEAT) {
            btnRepeat.getStyleClass().add("selected");
            Player.getInstance().setRepeatState(RepeatState.REPEAT_ALL);
        } else if(current == RepeatState.REPEAT_ALL){
            btnRepeat.getStyleClass().add("repeat-one");
            Player.getInstance().setRepeatState(RepeatState.REPEAT_ONE);
        } else {
            btnRepeat.getStyleClass().removeAll("selected", "repeat-one");
            Player.getInstance().setRepeatState(RepeatState.NO_REPEAT);
        }
    }

    /**
     * Actualise l'affichage des trois informations de la piste en cours.
     */
    private void refreshSongInfo(){
        if(Player.getInstance().getState() == Observation.PLAYER_STARTED || Player.getInstance().getState() == Observation.PLAYER_PAUSED) {
            songTitle.setText(Player.getInstance().getSongTitle());
            songArtist.setText(Player.getInstance().getSongArtist());
            songAlbum.setText(Player.getInstance().getSongAlbum());
        } else
            eraseSongInfo();
    }

    /**
     * enleve les informations de la chanson sur l'interface
     */
    private void eraseSongInfo(){
        songTitle.setText("");
        songArtist.setText("");
        songAlbum.setText("");
    }

    @Override
    public void update(Observation argument){
        switch (argument) {
            case PLAYLIST_SET:
                btnPlay.setDisable(false);
                btnNext.setDisable(false);
                btnPrevious.setDisable(false);
                btnStop.setDisable(false);
                btnRepeat.setDisable(false);
                btnShuffle.setDisable(false);
                volumeSlider.setDisable(false);
                refreshSongInfo();
                break;
            case NOTHING_TO_PLAY:
                btnPlay.setDisable(true);
                btnNext.setDisable(true);
                btnPrevious.setDisable(true);
                btnStop.setDisable(true);
                btnRepeat.setDisable(true);
                btnShuffle.setDisable(true);
                volumeSlider.setDisable(true);
                eraseSongInfo();
                break;
            case SONG_LOADED:
            case SONG_REMOVED_OR_ADDED:
                refreshSongInfo();
                break;
            case PLAYER_STARTED:
                setInterfacePlaying();
                refreshSongInfo();
                break;
            case PLAYER_PAUSED:
                setInterfaceNotPlaying();
                refreshSongInfo();
        }
    }
}
