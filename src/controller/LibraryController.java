package controller;

import interfaces.Observation;
import interfaces.Observer;
import javafx.animation.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import model.*;

import java.net.URL;
import java.nio.file.Paths;
import java.util.*;

/**
 * Contrôleur de l'onglet Biliothèque («Library»).
 */
public class LibraryController implements Initializable, Observer {
    private static final int GRID_IMAGE_SIZE = 180;

    private static final Duration TRANSITION_DURATION = Duration.millis(400);

    private static final Duration FAST_TRANSITION_DURATION = Duration.millis(200);

    private static final int GRID_SPACING = 20;

    private static final Interpolator TRANSITION_INTERPOLATOR = Interpolator.SPLINE(0.5, 0, 1, 0.7);

    private static final Color UNSELECTED_ITEM_COLOR = Color.rgb(0x00, 0x93, 0xff);

    private Player player = Player.getInstance();

    /** Tableau pour la vue de listes de chansons. */
    @FXML
    private TableView tblLibrary;
    @FXML
    private TableColumn colLibrarySongName;
    @FXML
    private TableColumn colLibrarySongArtist;
    @FXML
    private TableColumn colLibrarySongAlbum;
    @FXML
    private TableColumn colLibrarySongLenght;

    /**
     * Tableau pour la vue de listes de playlists.
     */
    @FXML
    private TableView tblPlaylist;
    @FXML
    private TableColumn colPlaylistName;
    @FXML
    private TableColumn colPlaylistCreationDate;

    @FXML
    private StackPane playlistsItem;
    @FXML
    private Label playlistsItemText;
    @FXML
    private StackPane songsItem;
    @FXML
    private Label songsItemText;
    @FXML
    private StackPane artistsItem;
    @FXML
    private Label artistsItemText;
    @FXML
    private StackPane albumsItem;
    @FXML
    private Label albumsItemText;

    /** Item sélectionné. */
    private Label currentNavigationItem;

    /**
     * Grille affichée pour les dispositions d'éléments où cela convient (albums, etc.)
     */
    @FXML
    private GridPane gridLibrary;

    @FXML
    private StackPane libraryContentWrapper;

    private ResourceBundle bundle;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bundle = resources;

        colLibrarySongAlbum.setCellValueFactory(new PropertyValueFactory<Song, String>("album"));
        colLibrarySongArtist.setCellValueFactory(new PropertyValueFactory<Song, String>("artist"));
        colLibrarySongName.setCellValueFactory(new PropertyValueFactory<Song, String>("title"));
        colLibrarySongLenght.setCellValueFactory(new PropertyValueFactory<Song, String>("timeString"));
        tblLibrary.setItems(Library.getInstance().getSongList());
        resetSongsTableMouseListener();

        tblLibrary.setOnMousePressed(mouseEvent -> {
            if (mouseEvent.isPrimaryButtonDown() && mouseEvent.getClickCount() == 2) {
                if (Player.getInstance().getState() == Observation.PLAYER_STARTED) {
                    Player.getInstance().stop();
                }
                Song selectedItem = getCurrentSelectedSong();
                if (selectedItem != null) {
                    ArrayList<MediaItem> playlist = new ArrayList<>();
                    playlist.add(selectedItem);
                    Player.getInstance().setPlaylist(new Playlist(bundle.getString("new-playlist"), playlist));
                    Player.getInstance().togglePlay();
                }
            }
        });

        colPlaylistName.setCellValueFactory(new PropertyValueFactory<Playlist, String>("playlistName"));
        colPlaylistCreationDate.setCellValueFactory(new PropertyValueFactory<Playlist, String>("creationDate"));
        tblPlaylist.setItems(Util.getPlaylists());

        tblPlaylist.setOnMousePressed(mouseEvent -> {
            if (mouseEvent.isPrimaryButtonDown() && mouseEvent.getClickCount() == 2) {
                if (Player.getInstance().getState() == Observation.PLAYER_STARTED) {
                    Player.getInstance().stop();
                }
                Player.getInstance().setPlaylist((Playlist) tblPlaylist.getSelectionModel().getSelectedItem());
            }
        });

        gridLibrary.setHgap(GRID_SPACING);
        gridLibrary.setVgap(GRID_SPACING);
        gridLibrary.setPadding(new Insets(GRID_SPACING, GRID_SPACING, GRID_SPACING, GRID_SPACING));

        playlistsItem.setOnMouseClicked(event -> {
            turnDisplayToPlaylistTable();
            if(currentNavigationItem != playlistsItemText) {
                unselectCurrentNavigationItem();
                selectNavigationItem(playlistsItemText);
            }
        });
        songsItem.setOnMouseClicked(event -> {
            turnDisplayToSongsTable();
            if(currentNavigationItem != songsItemText) {
                unselectCurrentNavigationItem();
                selectNavigationItem(songsItemText);
            }
        });
        artistsItem.setOnMouseClicked(event -> {
            turnDisplayToGrid(GridDisplay.ARTISTS);
            if(currentNavigationItem != artistsItemText) {
                unselectCurrentNavigationItem();
                selectNavigationItem(artistsItemText);
            }
        });
        albumsItem.setOnMouseClicked(event -> {
            turnDisplayToGrid(GridDisplay.ALBUMS);
            if(currentNavigationItem != albumsItemText) {
                unselectCurrentNavigationItem();
                selectNavigationItem(albumsItemText);
            }
        });

        selectNavigationItem(songsItemText);

        Player.getInstance().addObserver(this);
    }

    private void selectNavigationItem(Label label){
        Timeline animation = new Timeline(new KeyFrame(Duration.ZERO, new KeyValue(label.textFillProperty(), UNSELECTED_ITEM_COLOR)),
                new KeyFrame(FAST_TRANSITION_DURATION,
                    new KeyValue(label.textFillProperty(), Color.BLACK, Interpolator.EASE_BOTH),
                    new KeyValue(label.scaleXProperty(), 1.4, Interpolator.EASE_BOTH),
                    new KeyValue(label.scaleYProperty(), 1.4, Interpolator.EASE_BOTH)
                )
        );

        if(!label.getStyleClass().contains("selected"))
            label.getStyleClass().add("selected");

        animation.play();

        currentNavigationItem = label;
    }

    private void unselectCurrentNavigationItem(){
        Timeline animation = new Timeline(new KeyFrame(FAST_TRANSITION_DURATION,
                new KeyValue(currentNavigationItem.textFillProperty(), UNSELECTED_ITEM_COLOR, Interpolator.EASE_BOTH),
                new KeyValue(currentNavigationItem.scaleXProperty(), 1, Interpolator.EASE_BOTH),
                new KeyValue(currentNavigationItem.scaleYProperty(), 1, Interpolator.EASE_BOTH)
        ));

        currentNavigationItem.getStyleClass().remove("selected");

        animation.play();
    }

    /**
     * Affiche la vue sous forme de tableau.
     */
    private void turnDisplayToSongsTable(){
        if (!tblLibrary.isVisible()) {
            gridLibrary.setVisible(false);
            tblPlaylist.setVisible(false);
            tblLibrary.setVisible(true);
        }
        tblLibrary.setItems(Library.getInstance().getSongList());
    }

    private void resetSongsTableMouseListener(){
        SelectionModel selectionModel = tblLibrary.getSelectionModel();
        selectionModel.selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(selectionModel.isEmpty())
                tblLibrary.setContextMenu(null);
            else
                tblLibrary.setContextMenu(createSongsContextMenu());
        });
    }

    /**
     * Affiche l'une des deux vues en grille disponibles, soit la vue des artistes soit celle des albums.
     *
     * @param display Grille à afficher.
     */
    private void turnDisplayToGrid(GridDisplay display) {
        HashMap<String, ImageView> gridItems = new HashMap<>();

        if (!gridLibrary.isVisible()) {
            gridLibrary.setVisible(true);
            tblLibrary.setVisible(false);
            tblPlaylist.setVisible(false);
        }

        for (Song song : Library.getInstance().getSongList()) {
            String identifier = display == GridDisplay.ARTISTS ? song.getArtist() : song.getAlbum();

            if (!gridItems.containsKey(identifier)) {
                if (song.getImage() == null) {
                    gridItems.put(identifier, new ImageView(Paths.get("resources/MichaelJacksonBadCover.jpg").toString()));
                } else {
                    gridItems.put(identifier, new ImageView(song.getImage()));
                }
            } else if (song.getImage() != null && gridItems.get(identifier).equals(
                    new ImageView(Paths.get("resources/MichaelJacksonBadCover.jpg").toString()))) {
                gridItems.replace(identifier, new ImageView(song.getImage()));
            }
        }

        setGridItems(gridItems.entrySet(), display);
    }

    private void turnDisplayToPlaylistTable() {
        if (!tblPlaylist.isVisible()) {
            gridLibrary.setVisible(false);
            tblLibrary.setVisible(false);
            tblPlaylist.setVisible(true);
        }
    }

    private void setGridItems(Set<Map.Entry<String, ImageView>> entrySet, GridDisplay display) {
        int gridX = 0;
        int gridY = 0;

        gridLibrary.getChildren().clear();

        for (Map.Entry<String, ImageView> entry : entrySet) {
            final int thisX = gridX;
            final int thisY = gridY;

            final ObservableList thisItemsSongs = FXCollections.observableArrayList();
            Song compareSong = new Song();
            if (display == GridDisplay.ARTISTS) {
                compareSong.setArtist(entry.getKey());
                thisItemsSongs.addAll(Library.getInstance().getSongsWithSameArtist(compareSong));
            } else {
                compareSong.setAlbum(entry.getKey());
                thisItemsSongs.addAll(Library.getInstance().getSongsWithSameAlbum(compareSong));
            }

            final VBox pane = new VBox();

            entry.getValue().fitHeightProperty().bind(pane.heightProperty());
            entry.getValue().fitWidthProperty().bind(pane.widthProperty());
            pane.getChildren().add(entry.getValue());
            pane.getChildren().add(new Label(entry.getKey()));
            pane.setAlignment(Pos.TOP_CENTER);
            pane.setMaxSize(GRID_IMAGE_SIZE, GRID_IMAGE_SIZE);
            pane.setPrefSize(GRID_IMAGE_SIZE, GRID_IMAGE_SIZE);
            pane.setMinSize(GRID_IMAGE_SIZE, GRID_IMAGE_SIZE);

            pane.setOnMouseClicked(event -> {
                if (event.getButton() == MouseButton.PRIMARY) {
                    tblLibrary.setItems(thisItemsSongs);
                    switchToTableViewFromGridImageView(entry.getValue(), thisX, thisY);
                }
            });

            pane.setOnContextMenuRequested(event -> {
                ContextMenu menu = createReadAllContextMenu(thisItemsSongs, entry.getKey());
                menu.show(pane, event.getScreenX(), event.getScreenY());
            });

            gridLibrary.add(pane, gridX, gridY);

            gridX++;
            if (gridX > 4) {
                gridX = 0;
                gridY++;
            }
        }
    }

    private void switchToTableViewFromGridImageView(ImageView image, int gridXPos, int gridYPos) {
        FadeTransition fadeTransition = new FadeTransition(TRANSITION_DURATION);
        fadeTransition.setFromValue(1);
        fadeTransition.setToValue(0);

        ScaleTransition scaleTransition = new ScaleTransition(TRANSITION_DURATION);
        scaleTransition.setToX(6);
        scaleTransition.setToY(6);

        image.fitHeightProperty().unbind();
        image.fitWidthProperty().unbind();

        image.setTranslateX(GRID_SPACING + gridXPos * (GRID_IMAGE_SIZE + GRID_SPACING));
        image.setTranslateY(GRID_SPACING + gridYPos * (GRID_IMAGE_SIZE + GRID_SPACING));

        gridLibrary.getChildren().clear();
        gridLibrary.setVisible(false);
        libraryContentWrapper.getChildren().add(image);

        ParallelTransition parallelTransition = new ParallelTransition(image, fadeTransition, scaleTransition);
        parallelTransition.setInterpolator(TRANSITION_INTERPOLATOR);
        parallelTransition.play();

        parallelTransition.setOnFinished(event -> {
            libraryContentWrapper.getChildren().remove(image);
            gridLibrary.setOpacity(1);
            tblLibrary.setVisible(true);
        });
    }

    /**
     * Affiche un menu contextuel permettant de changer la liste de lecture pour y mettre une liste de chansons.
     *
     * @param songs Chansons à ajouter.
     */
    private ContextMenu createReadAllContextMenu(ObservableList songs, String newPlaylistTitle){
        ContextMenu songsContextMenu = new ContextMenu();
        MenuItem mnuPlayAll = new MenuItem(bundle.getString("playAll"));
        mnuPlayAll.setOnAction(event1 -> {
            ArrayList<MediaItem> playlist = new ArrayList<>();
            playlist.addAll(songs);
            player.setPlaylist(new Playlist(newPlaylistTitle, playlist));
            player.togglePlay();
        });
        songsContextMenu.getItems().addAll(mnuPlayAll);

        return songsContextMenu;
    }

    private ContextMenu createSongsContextMenu(){
        Song selectedSong = getCurrentSelectedSong();
        ContextMenu menu = new ContextMenu();

        tblLibrary.getSelectionModel().getSelectedCells();

        MenuItem playThis = new MenuItem(bundle.getString("playSongPart1")
                + selectedSong.getTitle() + bundle.getString("playSongMenuClosingQuote"));
        playThis.setOnAction(event -> {
            player.setSingleSongPlaylist(selectedSong);
            player.togglePlay();
        });

        MenuItem playAlbum = new MenuItem(bundle.getString("playAlbumPart1")
                + selectedSong.getAlbum() + bundle.getString("playSongMenuClosingQuote"));
        playAlbum.setOnAction(event -> {
            player.setPlaylistFromAlbumOf(selectedSong);
            player.togglePlay();
        });

        MenuItem playArtist = new MenuItem(bundle.getString("playArtistPart1")
                + selectedSong.getArtist() + bundle.getString("playSongMenuClosingQuote"));
        playArtist.setOnAction(event -> {
            player.setPlaylistFromArtistOf(selectedSong);
            player.togglePlay();
        });

        MenuItem addToCurrentPlaylist = new MenuItem(bundle.getString("addToCurrentpart1") + selectedSong.getTitle() +
                bundle.getString("playSongMenuClosingQuote") +
                bundle.getString("addToCurrentpart2"));

        addToCurrentPlaylist.setOnAction(event -> {
            player.getPlaylist().addMedia(selectedSong);
            player.updatePlaylist();
        });

        menu.getItems().addAll(playThis, playAlbum, playArtist, addToCurrentPlaylist);

        return menu;
    }

    private Song getCurrentSelectedSong(){
        return (Song) tblLibrary.getSelectionModel().getSelectedItem();
    }

    @Override
    public void update(Observation argument) {
        switch (argument) {
            case SONG_METADATA_UPDATED:
                tblLibrary.refresh();
        }
    }

    /**
     * Possibilités de disposition en grille.
     */
    private enum GridDisplay {
        ARTISTS, ALBUMS
    }
}
