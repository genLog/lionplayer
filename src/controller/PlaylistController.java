package controller;

import common.Common;
import interfaces.Observation;
import interfaces.Observer;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import view.Dialogs;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Vue de la liste de lecture dans la barre latérale de droite.
 */
public class PlaylistController implements Initializable, Observer {
    private static Logger log = LoggerFactory.getLogger(PlaylistController.class);

    private static final DataFormat DRAG_DROP_DATA_FORMAT = new DataFormat("application/octet-stream");

    public static final String STYLECLASS_MOVE_TOP = "list-item-border-top";
    public static final String STYLECLASS_MOVE_BOTTOM = "list-item-border-bottom";

    @FXML
    private ListView playlistView;
    @FXML
    private TextField playlistName;

    private ResourceBundle bundle;

    final EventHandler<KeyEvent> keyEventHandler = (final KeyEvent keyEvent) -> {
        if (keyEvent.getCode() == KeyCode.ENTER) {
            if (playlistName.isEditable()) {
                Player.getInstance().getPlaylist().setPlaylistName(playlistName.getText());
                playlistName.setEditable(false);
            }
            keyEvent.consume();
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bundle = resources;

        playlistView.setOnMouseClicked(clickEvent -> {
            if (clickEvent.getClickCount() == 2) {
                if (Player.getInstance().getState() == Observation.PLAYER_STARTED) {
                    Player.getInstance().stop();
                    if (Player.getInstance().isShuffleOn())
                        Player.getInstance().turnOnShuffle();
                }

                int selectedIndex = playlistView.getSelectionModel().getSelectedIndex();
                if (selectedIndex >= 0) {
                    Player.getInstance().setPlayingSongId(selectedIndex);
                }
            }
        });

        playlistName.setOnKeyPressed(keyEventHandler);

        initializePlaylistContextMenu();

        Player.getInstance().addObserver(this);
    }

    /**
     * met a jour les informations de la playlist dans le UI
     *
     * @param playlist la playlist
     */
    private void setPlaylistInfo(Playlist playlist) {
        playlistView.setCellFactory(listView -> new DraggableListCell());
        playlistView.getItems().setAll(playlist.getMediaItems());
        playlistName.setText(playlist.getPlaylistName());
    }

    /**
     * Initialise le menu contextuel pour la playlist
     */
    private void initializePlaylistContextMenu() {
        ContextMenu playlistContextMenu = new ContextMenu();
        MenuItem mnuAdd = new MenuItem(bundle.getString("add"));
        MenuItem mnuRemove = new MenuItem(bundle.getString("remove"));
        MenuItem mnuRename = new MenuItem(bundle.getString("rename"));
        MenuItem mnuEditTag = new MenuItem(bundle.getString("edit-tags"));
        mnuAdd.setOnAction(event -> doAddSongsAction());
        mnuRemove.setOnAction(event -> removeMedia());
        mnuRename.setOnAction(event -> renamePlaylist());
        mnuEditTag.setOnAction(event -> editTag());
        playlistContextMenu.getItems().addAll(mnuAdd, mnuRename, mnuEditTag, new SeparatorMenuItem(), mnuRemove);
        playlistView.setContextMenu(playlistContextMenu);
    }

    /**
     * Ouvre la fenetre d'edition d'étiquette
     */
    private void editTag() {
        Playlist playlist = Player.getInstance().getPlaylist();
        if (playlist != null && playlistView.getSelectionModel().getSelectedIndex() != -1) {
            MediaItem selectedSong = playlist.mediaAt(playlistView.getSelectionModel().getSelectedIndex());
            try {
                Parent root;
                FXMLLoader loader = new FXMLLoader();
                loader.setResources(bundle);
                loader.setLocation(TagEditorController.class.getResource(Common.EDITOR_WINDOW_FXML));
                root = loader.load();

                Stage stage = new Stage();
                stage.setTitle(bundle.getString("tag-editor"));
                stage.setScene(new Scene(root));
                ((TagEditorController) loader.getController()).setStage(stage);
                ((TagEditorController) loader.getController()).loadMetadata((Song)selectedSong);
                stage.show();
            } catch (IOException e) {
                log.error("Edit tag io exception", e);
            }
        }
    }

    /**
     * permet a l'utilisateur de changer le nom de la playlist
     */
    private void renamePlaylist() {
        playlistName.setEditable(true);
    }

    /**
     * Retire une chanson de la playlist.
     */
    private void removeMedia() {
        if (Player.getInstance().getPlaylist() != null && playlistView.getSelectionModel().getSelectedIndex() != -1) {
            MediaItem oldMedia = Player.getInstance().getPlaylist().mediaAt(playlistView.getSelectionModel().getSelectedIndex());
            Player.getInstance().getPlaylist().removeMedia(oldMedia);
            Player.getInstance().updatePlaylist();
        }
    }

    /**
     * Ouvre un sélecteur de fichiers et permet de choisir des pistes à ajouter à la liste de lecture.
     */
    private void doAddSongsAction() {
        List<MediaItem> selectedFiles = Dialogs.getInstance().openFileChooser();
        String path;

        if (selectedFiles != null) {
            Library.getInstance().addAllMedia(selectedFiles);
            path = Util.getOs();
            File file = new File(path + File.separator + Common.LIBRARY_SAVE);
            if (file != null) {
                Library.getInstance().save(file);
            }

            if (Player.getInstance().getPlaylist() == null) {
                Player.getInstance().setPlaylist(new Playlist(bundle.getString("new-playlist"), selectedFiles));
            } else {
                Player.getInstance().getPlaylist().addAllMedia(selectedFiles);
                Player.getInstance().updatePlaylist();
            }
        }
    }

    /**
     * update lorsque le modele change
     *
     * @param argument de modification, represente ce qui a ete modifie
     */
    @Override
    public void update(Observation argument) {
        switch (argument) {
            case PLAYLIST_SET:
            case SONG_REMOVED_OR_ADDED:
            case SONG_METADATA_UPDATED:
            case PLAYER_STARTED:
                setPlaylistInfo(Player.getInstance().getPlaylist());
                break;
        }
    }

    private class DraggableListCell extends ListCell<MediaItem> {

        private ImageView imageView = new ImageView();

        private DraggableListCell() {
            setOnDragDetected(event -> {
                if (getItem() != null) {
                    Dragboard dragboard = startDragAndDrop(TransferMode.MOVE);
                    HashMap<DataFormat, Object> content = new HashMap();
                    content.put(DRAG_DROP_DATA_FORMAT, "");
                    dragboard.setContent(content);
                }

                event.consume();
            });

            setOnDragOver(event -> {
                if (isValidDragEvent(event))
                    event.acceptTransferModes(TransferMode.MOVE);

                event.consume();
            });

            setOnDragEntered(event -> {
                if (isValidDragEvent(event)) {
                    placeBorderAccordingToMousePos(event);

                    ((Node) event.getGestureSource()).setOpacity(0.4);
                }

                event.consume();
            });

            setOnDragExited(event -> {
                if (isValidDragEvent(event)) {
                    getStyleClass().remove(STYLECLASS_MOVE_TOP);
                    getStyleClass().remove(STYLECLASS_MOVE_BOTTOM);

                    ((Node) event.getGestureSource()).setOpacity(1);
                }

                event.consume();
            });

            setOnDragDropped(event -> {
                if (isValidDragEvent(event)) {
                    MediaItem sourceMedia = ((DraggableListCell) event.getGestureSource()).getItem();
                    Playlist playlist = Player.getInstance().getPlaylist();

                    if (isTargetOverSource(event))
                        playlist.moveBefore(sourceMedia, getItem());
                    else
                        playlist.moveAfter(sourceMedia, getItem());

                    setPlaylistInfo(Player.getInstance().getPlaylist());

                    event.setDropCompleted(true);
                } else {
                    event.setDropCompleted(false);
                }

                event.consume();
            });

            setOnDragDone(DragEvent::consume);
        }

        private boolean isValidDragEvent(DragEvent event) {
            return event.getGestureSource() != this && getItem() != null
                    && event.getDragboard().hasContent(DRAG_DROP_DATA_FORMAT);
        }

        private void placeBorderAccordingToMousePos(DragEvent event) {
            if (isTargetOverSource(event)) {
                if (!getStyleClass().contains(STYLECLASS_MOVE_TOP))
                    getStyleClass().add(STYLECLASS_MOVE_TOP);
            } else {
                if (!getStyleClass().contains(STYLECLASS_MOVE_BOTTOM))
                    getStyleClass().add(STYLECLASS_MOVE_BOTTOM);
            }
        }

        /**
         * Détermine si l'item d'arrivée est au-dessus, dans la liste, de l'item déplacé.
         *
         * @param event Événement de glisser-déposer.
         * @return Vrai si l'item d'arrivée est au-dessus de l'item déplacé.
         */
        private boolean isTargetOverSource(DragEvent event) {
            MediaItem sourceMedia = ((DraggableListCell) event.getGestureSource()).getItem();
            Playlist playlist = Player.getInstance().getPlaylist();

            return playlist.indexOf(sourceMedia) > playlist.indexOf(getItem());
        }


        protected void updateItem(MediaItem song, boolean empty) {
            super.updateItem(song, empty);
            setMaxWidth(getListView().getMaxWidth());

            if (empty || song == null) {
                setText(null);
                setGraphic(null);
            } else if (song == Player.getInstance().getCurrentMedia()) {
                Image image = new Image(getClass().getResource(Common.PLAYING_ICON).toString(),true);
                imageView.setImage(image);
                setText(song.getTitle() + Common.SEPARATOR + song.getArtist());
                setGraphic(imageView);
            } else {
                setText(song.getTitle() + Common.SEPARATOR + song.getArtist());
                setGraphic(new Rectangle(20, 20, Color.TRANSPARENT));
            }
        }
    }
}
