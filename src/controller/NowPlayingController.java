package controller;

import interfaces.Observation;
import interfaces.Observer;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.media.MediaView;
import model.Player;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Contrôleur de l'onglet «En cours» («Now Playing»).
 */
public class NowPlayingController implements Initializable, Observer {
    /** Image à afficher au milieu. */
    @FXML
    private ImageView songCoverArt;

    /** Conteneur de l'image. */
    @FXML
    private StackPane nowPlayingPane;

    /** Controlleur de la video */
    @FXML
    private MediaView videoView;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        nowPlayingPane.resize(1, 1); // Patch pour le bug 2
        songCoverArt.fitHeightProperty().bind(nowPlayingPane.heightProperty().divide(1.2));
        songCoverArt.fitWidthProperty().bind(nowPlayingPane.widthProperty().divide(1.2));
        videoView.fitHeightProperty().bind(nowPlayingPane.heightProperty());
        videoView.fitWidthProperty().bind(nowPlayingPane.widthProperty());
        Player.getInstance().addObserver(this);
    }

    @Override
    public void update(Observation argument) {
        switch (argument) {
            case PLAYLIST_SET:
            case SONG_LOADED:
                songCoverArt.setImage(Player.getInstance().getSongCoverArt());
                videoView.setMediaPlayer(Player.getInstance().getMediaPlayer());
                break;
            case NOTHING_TO_PLAY:
                songCoverArt.setImage(null);
        }
    }
}
