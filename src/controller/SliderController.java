package controller;

import interfaces.Observation;
import interfaces.Observer;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Slider;
import model.Player;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * controlleur pour le slider de l'avancement de la piste
 *
 * @author Maxim Bernard
 * @since 9/15/2015.
 */
public class SliderController implements Initializable, Observer {
    @FXML
    private Slider mainSlider;

    /** Thread qui déplace le bouton du slider pour suivre la progression du lecteur. */
    private Thread progressionThread;

    /** Vrai si le thread {@link #progressionThread} est en cours. */
    private boolean isSliderMoving;

    /** Vrai juste avant que la valeur du slider ne change automatiquement (pour ne pas
     *  déclencher l'événement lors du changement de valeur). */
    private boolean isSliderGoingToMove;

    /**
     * À intervalles réguliers, actualise la valeur du slider selon la progression
     * du lecteur.
     */
    private Runnable progressionMethod = () -> {
        while(isSliderMoving){
            try {
                Thread.sleep(200);

                Platform.runLater(() -> {
                    isSliderGoingToMove = true;
                    mainSlider.setValue(Player.getInstance().getCurrentSongPosition());
                    isSliderGoingToMove = false;
                });
            } catch (InterruptedException ex) {} // Ignorer l'exception
        }
    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mainSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (!isSliderGoingToMove)
                Player.getInstance().seekPosition(newValue.doubleValue());
        });

        Player.getInstance().addObserver(this);
    }

    /**
     * Démarre le mouvement du bouton du slider selon la progression de la piste en cours.
     */
    private void startFollowingProgression(){
        if(progressionThread != null)
            progressionThread.interrupt();

        isSliderMoving = true;
        progressionThread = new Thread(progressionMethod);
        progressionThread.start();
    }

    /**
     * Arrête le mouvement du slider.
     */
    private void stopFollowingProgression(){
        isSliderMoving = false;
        if(progressionThread != null) {
            progressionThread.interrupt();
            progressionThread = null;
        }
    }

    /**
     * Ramène le slider au début.
     */
    private void resetToZero(){
        isSliderGoingToMove = true;
        mainSlider.setValue(0);
        isSliderGoingToMove = false;
    }

    @Override
    public void update(Observation argument){
        switch (argument) {
            case PLAYLIST_SET:
                mainSlider.setDisable(false);
                break;
            case NOTHING_TO_PLAY:
                mainSlider.setDisable(true);
                break;
            case PLAYER_STARTED:
                startFollowingProgression();
                break;
            case PLAYER_STOPPED:
                resetToZero();
                // fallthrough
            case PLAYER_PAUSED:
                stopFollowingProgression();
        }
    }
}
