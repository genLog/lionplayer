package interfaces;

/**
 * Paramètre pouvant être passé lors de la notification des observateurs.
 */
public enum Observation {
    /**
     * Indique qu'au moins une piste se trouve dans la liste de lecture en cours.
     */
    PLAYLIST_SET,

    /**
     * Indique qu'aucune piste ne se trouve dans la liste de lecture en cours.
     */
    NOTHING_TO_PLAY,

    /**
     * Indique que la lecture a été arrêtée ou mise en pause.
     */
    PLAYER_PAUSED,

    /**
     * Indique que la lecture a été arrêtée.
     */
    PLAYER_STOPPED,

    /**
     * Indique qu'une nouvelle piste est en cours de lecture.
     */
    PLAYER_STARTED,

    /**
     * Indique qu'une piste a été chargée.
     */
    SONG_LOADED,

    /**
     * Indique qu'une piste à été enlevée.
     */
    SONG_REMOVED_OR_ADDED,

    /**
     * Indique que les métadonnées d'une chanson ont été changées.
     */
    SONG_METADATA_UPDATED,

    /**
     * Media chargé
     */
    PLAYER_READY,

    /**
     * Erreur lors du chargement media
     */
    PLAYER_ERROR,

    /**
     * Fin du média atteind
     */
    PLAYBACK_REACHED_END_OF_MEDIA
}
