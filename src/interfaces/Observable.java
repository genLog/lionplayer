package interfaces;

/**
 * Objet dont les changements d'état peuvent être observés par des {@link Observer}.
 */
public interface Observable {
    /**
     * Ajoute un observateur.
     * @param observer Observateur à ajouter.
     */
    void addObserver(Observer observer);

    /**
     * Notifie tous les observateurs d'un changement d'état.
     */
    void notifyObservers(Observation argument);
}
