package interfaces;

/**
 * Objet pouvant observer les changements d'état d'un {@link Observable}.
 */
public interface Observer {
    /**
     * Méthode appelée par l'{@link Observable} lorsqu'un changement d'état a lieu.
     */
    void update(Observation argument);
}
