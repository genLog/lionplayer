package common;

import model.Playlist;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.*;

/**
 * Tests.
 * @author Marc-Andr�
 * @since 29/09/2015.
 */
public class PreferenceTest {

    /**
     * Deletes all preferences
     */
    @Before
    public void setUp() {
        if(!Preference.clear()){
            System.out.println("Failed to clear preferences");
            fail();
        }
    }

    /**
     * Deletes all preferences after running
     */
    @AfterClass
    public static void tearDownClass(){
        Preference.clear();
    }

    /**
     * Creates a temp folder and delete it for every test
     */
    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    /**
     * Tests that the last directory preference wont take a null file
     */
    @Test
    public void testLastOpenedDirectoryWithNull() {
        File fileDefault = Preference.getLastOpenedDirectory();
        File nullFile = null;
        Preference.putLastOpenedDirectory(nullFile);

        assertNotNull(fileDefault);
        assertNotNull(Preference.getLastOpenedDirectory());
        assertEquals(fileDefault, Preference.getLastOpenedDirectory());
    }

    /**
     * Tests that the last directory preference will actually return a valid last folder
     */
    @Test
    public void testLastOpenedDirectoryWithFolder() {
        File fileDefault = Preference.getLastOpenedDirectory();
        try {
            File userFolder = folder.newFolder("userfolder");
            Preference.putLastOpenedDirectory(userFolder);
            assertNotEquals(fileDefault, Preference.getLastOpenedDirectory());
            assertEquals(userFolder, Preference.getLastOpenedDirectory());
        } catch (IOException e) {
            System.out.println("Fake folder failed");
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Tests that the last directory preference wont user a deleted folder
     */
    @Test
    public void testLastOpenedDirectoryWithDeletedFolder() {
        File fileDefault = Preference.getLastOpenedDirectory();
        try {
            File userFolder = folder.newFolder("userfolder");
            Preference.putLastOpenedDirectory(userFolder);
            userFolder.delete();
            assertEquals(fileDefault, Preference.getLastOpenedDirectory());
            assertNotEquals(userFolder, Preference.getLastOpenedDirectory());
        } catch (IOException e) {
            System.out.println("Fake folder failed");
            e.printStackTrace();
            fail();
        }
    }

    /**
     * Tests that the last directory preference wont take a file thats not a directory
     */
    @Test
    public void testLastOpenedDirectoryWithFile() {
        File fileDefault = Preference.getLastOpenedDirectory();
        try {
            File userFile = folder.newFile("userfolder");
            Preference.putLastOpenedDirectory(userFile);
            assertEquals(fileDefault, Preference.getLastOpenedDirectory());
            assertNotEquals(userFile, Preference.getLastOpenedDirectory());
        } catch (IOException e) {
            System.out.println("Fake file failed");
            e.printStackTrace();
            fail();
        }
    }


    /**
     * Tests that the last set volume wont take values above max
     */
    @Test
    public void testLastSetVolumeOver() {
        double soundDefault = Preference.getLastSetVolume();
        Preference.putLastSetVolume(Common.VOLUME_MAX+0.1);
        assertEquals(soundDefault,Preference.getLastSetVolume(),0);
        assertTrue(soundDefault<=Common.VOLUME_MAX);
    }

    /**
     * Tests that the last set volume wont take values below min
     */
    @Test
    public void testLastSetVolumeUnder() {
        double soundDefault = Preference.getLastSetVolume();
        Preference.putLastSetVolume(Common.VOLUME_MIN-0.01);
        assertEquals(soundDefault,Preference.getLastSetVolume(),0);
        assertTrue(soundDefault>=Common.VOLUME_MIN);
    }

    /**
     * Tests that the last set volume actually returns a valid changed value
     */
    @Test
    public void testLastSetVolumeChanged() {
        double soundDefault = Preference.getLastSetVolume();
        double nouveauVolume = 0.5;
        Preference.putLastSetVolume(nouveauVolume);
        assertNotEquals(soundDefault, nouveauVolume,0);
        assertNotEquals(soundDefault, Preference.getLastSetVolume(),0);
        assertEquals(nouveauVolume, Preference.getLastSetVolume(), 0);
    }

    /**
     * Tests that the last set volume percentage sends back the right percentage
     */
    @Test
    public void testLastSetVolumePourcentage() {
        Double soundDefault = Preference.getLastSetVolume();
        soundDefault *= 100;
        assertEquals(Preference.getLastSetVolumePercentage(),soundDefault.intValue());
    }

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    /**
     * No mather what the saved preference is for the last playlist it will trow a file not found exception if the file
     * does not exist
     */
    @Test
    public void testGetLastPlaylistChecksValid() throws Exception{
        Preference.putLastPlaylistPath("the impossible filename /\\?%*:|\"<>.");
        exception.expect(FileNotFoundException.class);
        Preference.getLastPlaylist();
    }

    /**
     * A loaded playlist should have the same stuff compared to the saved one.
     */
    @Test
    public void testGetLastPlaylistLoad(){
        try {
            Playlist testPlaylist = new Playlist("Nouvelle playlist");
            File lastPlaylist = folder.newFile("lastPL.lion");
            testPlaylist.save(lastPlaylist);
            assertEquals(Preference.getLastPlaylist().getCreationDate(), testPlaylist.getCreationDate());
            assertEquals(Preference.getLastPlaylist().getPlaylistLength(), testPlaylist.getPlaylistLength());
            assertEquals(Preference.getLastPlaylist().getPlaylistName(), testPlaylist.getPlaylistName());
            assertEquals(Preference.getLastPlaylist().getMediaItems(), testPlaylist.getMediaItems());

        } catch (IOException e) {
            fail("Could not create test file");
        }
    }
}