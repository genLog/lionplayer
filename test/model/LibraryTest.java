package model;

import common.Common;
import javafx.embed.swing.JFXPanel;
import model.exceptions.SongException;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by maxim on 15-10-13.
 */
public class LibraryTest {
    List<MediaItem> testList = new ArrayList<>();
    Library library;

    @Before
    public void setUp() throws SongException {
        new JFXPanel(); // Pour éviter l'erreur « Toolkit not initialized »

        try {
            Field libraryField = Library.class.getDeclaredField("instance");
            libraryField.setAccessible(true);
            libraryField.set(null, null);

            Constructor constructor = Library.class.getDeclaredConstructors()[0];
            constructor.setAccessible(true);
            libraryField.set(null, constructor.newInstance());
        } catch(NoSuchFieldException | IllegalAccessException | InstantiationException | InvocationTargetException ex){}

        library = Library.getInstance();

        testList.clear();
        testList.add(new Song(Common.PATH_TEST_URL1));
        testList.add(new Song(Common.PATH_TEST_URL2));
    }

    @Test
    public void testAddAllSongs(){
        library.addAllMedia(testList);

        List<Song> cmpList = library.getSongList();
        for(int i = 0; i < cmpList.size(); i++){
            assertEquals(testList.get(i), cmpList.get(i));
        }
    }

    @Test
    public void testGetSongsWithSameArtist() throws Exception {
        library.addAllMedia(testList);

        // Both have different artists
        assertEquals(library.getSongsWithSameArtist((Song)testList.get(0)).get(0), testList.get(0));
        assertEquals(library.getSongsWithSameArtist((Song)testList.get(1)).get(0), testList.get(1));
    }

    @Test
    public void testGetSongsWithSameAlbum() throws Exception {
        library.addAllMedia(testList);

        // Both have different albums
        assertEquals(library.getSongsWithSameAlbum((Song)testList.get(0)).get(0), testList.get(0));
        assertEquals(library.getSongsWithSameAlbum((Song)testList.get(1)).get(0), testList.get(1));
    }
}
