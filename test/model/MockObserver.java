package model;

import interfaces.Observation;
import interfaces.Observer;

/**
 * Classe permettant de verifier un appel a l'observateur
 *
 * @author Marc-André
 * @since 21/11/2015.
 */
public class MockObserver implements Observer, Runnable {
    Observation observation;
    private volatile boolean observerCalled;
    public volatile boolean shutdown;

    public MockObserver(Observation observation) {
        this.observation = observation;
        observerCalled = true;
    }

    @Override
    public synchronized  void update(Observation argument) {
        if (argument == observation) {
            observerCalled = false;
        }
    }

    @Override
    public void run() {
        while (observerCalled && !shutdown){}
    }
}
