package model;

import common.Common;
import interfaces.Observation;
import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import jdk.nashorn.internal.ir.annotations.Ignore;
import model.exceptions.SongException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Test de PlaybackManger.
 *
 * @author Marc-André Dion
 * @since 2015-11-18
 */
public class PlaybackManagerTest {
    private final static int TIMEOUT_MS = 10000; // Temps d'attente pour les changement d'état, augmenter sur ordi lent
    private final static double DOUBLE_THRESHOLD = 0.001;
    private PlaybackManager playbackManager;
    private Media bitrush2;
    private Media nonvalide;

    @Before
    public void setUp() throws Exception {
        new JFXPanel(); // Nécessaire pour les test de MediaPlayer
        playbackManager = new PlaybackManager();
        bitrush2 = new Media(getClass().getResource("/resources/BitRush2.mp3").toString());
        nonvalide = new Media(getClass().getResource("/resources/nonvalide.mp3").toString());
    }

    /**
     * Test l'observation pour l'état prêt du player
     *
     * @throws Exception
     */
    @Test
    public void testPlayerNotifyPlayerReadySiLoadMediaReussi() throws Exception {
        boolean estNotifier = chargerMediaAttendreNotify(Observation.PLAYER_READY, bitrush2, TIMEOUT_MS);
        assertEquals(false, estNotifier);
    }

    /**
     * Test la notification pour l'état d'erreur du lecteur
     *
     * @throws Exception
     */
    @Test
    public void testNotifyPlayerErrorSiLoadMediaEchoue() throws Exception {
        boolean estNotifier = chargerMediaAttendreNotify(Observation.PLAYER_ERROR, nonvalide, TIMEOUT_MS);
        assertEquals(false, estNotifier);
    }

    /**
     * Test que le lecteur demarre auto si le flag est setter
     * Test la notification pour l'état de lecture
     *
     * @throws Exception
     */
    @Test
    public void testLoadMediaAutoPlayWorks() throws Exception {
        boolean estNotifier = chargerMediaAttendreNotify(Observation.PLAYER_STARTED, bitrush2, TIMEOUT_MS, true);
        assertEquals(false, estNotifier);
    }

    /**
     * Test que le lecteur joue lorque le média est chargé et qu'on fait play
     * Test la notification pour l'état de lecture
     *
     * @throws Exception
     */
    @Test
    public void testNotifyPlayerStartedTogglePlayback() throws Exception {
        chargerMedia(bitrush2, false);
        attendreObservation(Observation.PLAYER_STARTED, () -> playbackManager.togglePlayback());
    }

    /**
     * Test que le lecteur pause s'il est en cours de lecture
     * Test la notification pour l'état de pause
     *
     * @throws Exception
     */
    @Test
    public void testNotifyPlayerPausedTogglePlayback() throws Exception {
        chargerMedia(bitrush2, true);
        attendreObservation(Observation.PLAYER_PAUSED, ()->playbackManager.togglePlayback());
    }

    /**
     * Test que le lecteur s'arrete
     * Test la notification pour l'état d'arret
     *
     * @throws Exception
     */
    @Test
    public void testNotifyPlayerStoppedStopPlayback() throws Exception {
        chargerMedia(bitrush2, true);
        attendreObservation(Observation.PLAYER_STOPPED, ()->playbackManager.stopPlayback());
    }

    /**
     * Test que le volume se modifie selon les attentes
     *
     * @throws Exception
     */
    @Test
    public void testLeVolumeChange() throws Exception {
        chargerMedia(bitrush2, true);

        // Verifie que le player set le volume max
        playbackManager.setVolume(Common.VOLUME_MAX);
        assertEquals(Common.VOLUME_MAX, playbackManager.getVolume(), DOUBLE_THRESHOLD);

        // Verifie la limite supérieur
        playbackManager.setVolume(Common.VOLUME_MAX+0.1);
        assertEquals(Common.VOLUME_MAX, playbackManager.getVolume(), DOUBLE_THRESHOLD);


        // Verifie que le player set le volume min
        playbackManager.setVolume(Common.VOLUME_MIN);
        assertEquals(Common.VOLUME_MIN, playbackManager.getVolume(), DOUBLE_THRESHOLD);

        // Verifie la limite inférieur
        playbackManager.setVolume(Common.VOLUME_MIN-0.1);
        assertEquals(Common.VOLUME_MIN, playbackManager.getVolume(), DOUBLE_THRESHOLD);

        // Verifie que le player set le volume voulu
        double nouveauVolume = 0.5;
        playbackManager.setVolume(nouveauVolume);
        assertEquals(nouveauVolume, playbackManager.getVolume(), DOUBLE_THRESHOLD);
    }

    /**
     * Charge un media.
     *
     * @param media    le media a charger
     * @param autoPlay si le media doit démarrer automatiquement
     * @throws Exception
     */
    private void chargerMedia(Media media, boolean autoPlay) throws Exception {
        chargerMediaAttendreNotify(Observation.PLAYER_READY, media, TIMEOUT_MS, autoPlay);
    }

    /**
     * Tente de charger un media et attend un état pour le temps donné.
     *
     * @param observation l'état attendu
     * @param media       le media a charger
     * @param timeout     le temps a attendre pour atteindre l'état
     * @return vrai si l'état à été atteind
     * @throws Exception
     */
    private boolean chargerMediaAttendreNotify(Observation observation, Media media, int timeout) throws Exception {
        return chargerMediaAttendreNotify(observation, media, timeout, false);
    }

    /**
     * Tente de charger un media et attend un état pour le temps donné.
     *
     * @param observation l'état attendu
     * @param media       le media a charger
     * @param timeout     le temps a attendre pour atteindre l'état
     * @param autoplay    indique si le lecteur demarre la lecture dès qu'il est prêt
     * @return vrai si l'état à été atteind
     * @throws Exception
     */
    private boolean chargerMediaAttendreNotify(Observation observation, Media media, int timeout, boolean autoplay) throws Exception {
        // Observateur sur un thread qui attend un appel avec l'argument spécifié et qui quitte quand c'est le cas
        MockObserver observer = new MockObserver(observation);
        Thread thread = new Thread(observer);
        thread.setDaemon(true); // Pour eviter les zombie
        thread.start();

        // Enregistre l'observateur comme un observateur de playbackManager
        playbackManager.addObserver(observer);

        // Charge un media
        playbackManager.loadMedia(media, false, autoplay);

        // Donne 500 ms au player pour atteindre l'état
        thread.join(timeout);
        return thread.isAlive();
    }

    @Test (expected = IllegalArgumentException.class)
    public void seekMauvaiseValeur() throws Exception{
        chargerMedia(bitrush2,false);
        playbackManager.seekMediaPosition(3);
    }

    public void attendreObservation(Observation etatAttendu, Runnable action) {
        // Observateur sur un thread qui attend un appel avec l'argument spécifié et qui quitte quand c'est le cas
        MockObserver observer = new MockObserver(etatAttendu);
        Thread thread = new Thread(observer);
        thread.setDaemon(true); // Pour eviter les zombie
        thread.start();

        playbackManager.addObserver(observer);

        action.run();

        try {
            thread.join(TIMEOUT_MS);
        } catch (InterruptedException e) {
            fail();
        }

        assertEquals(false, thread.isAlive());
    }
}