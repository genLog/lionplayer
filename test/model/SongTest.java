package model;

import common.Common;
import common.Preference;
import javafx.embed.swing.JFXPanel;
import model.exceptions.SongException;
import org.jaudiotagger.audio.exceptions.CannotWriteException;
import org.jaudiotagger.tag.FieldDataInvalidException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by mbiel on 9/16/2015.
 */
public class SongTest {
    static Song song;
    JFXPanel jfxPanel;

    @Before
    public void setUp() throws SongException {
        jfxPanel = new JFXPanel();
        song = new Song(Common.PATH_TEST_URL2);
    }

    /**
     * Deletes all preferences after running
     */
    @AfterClass
    public static void tearDownClass() throws FieldDataInvalidException, CannotWriteException {
        Preference.clear();
        song.setTitle("Project Yi Login");
        song.setAlbum("Lol");
        song.setArtist("League of Legends");
        song.setYear("2015");
        song.saveMetadata();
    }

    @Test
    public void testAjouterInfoNonNull() {
        assertEquals("Project Yi Login", song.getTitle());
        assertEquals("League of Legends", song.getArtist());
        assertEquals("2015", song.getYear());
        assertEquals(Common.PATH_TEST_URL2, song.getFile().toPath());
    }

    @Test
    public void testSaveMetadata() throws FieldDataInvalidException, CannotWriteException {
        assertEquals("Project Yi Login", song.getTitle());
        assertEquals("League of Legends", song.getArtist());
        assertEquals("Lol", song.getAlbum());
        assertEquals("2015", song.getYear());
        song.setTitle("testtitle");
        song.setAlbum("testalbum");
        song.setArtist("");
        song.setYear("2014");
        song.saveMetadata();
        assertEquals("testtitle", song.getTitle());
        assertEquals("testalbum", song.getAlbum());
        assertEquals("", song.getArtist());
        assertEquals("2014", song.getYear());
    }
}
