package model;

import common.Common;
import common.Preference;
import javafx.embed.swing.JFXPanel;
import model.exceptions.SongException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by mbiel on 9/16/2015.
 */
public class PlaylistTest {
    JFXPanel jfxPanel;
    Player player;
    Song song1;
    Song song2;
    Playlist playlist;

    @Before
    public void setUp() throws SongException {
        jfxPanel = new JFXPanel();
        player = Player.getInstance();
        song1 = new Song(Common.PATH_TEST_URL1);
        song2 = new Song(Common.PATH_TEST_URL2);
        playlist = new Playlist("Playlist");
    }

    /**
     * Deletes all preferences after running
     */
    @AfterClass
    public static void tearDownClass(){
        Preference.clear();
    }

    @Rule public TemporaryFolder tempFolder = new TemporaryFolder();

    @Test
    public void testNomDeLaPlaylist(){
        assertEquals("Playlist", playlist.getPlaylistName());
    }

    @Test
    public void testAjoutPlaylist(){
        assertEquals(0, playlist.getPlaylistLength());
        playlist.addMedia(song1);
        assertEquals(1, playlist.getPlaylistLength());
    }

    /**
     * Verifie qu'enlever une chanson marche
     */
    @Test
    public void testRemoveSongEnleve(){
        addAllSongsToPlaylist();
        playlist.removeMedia(song1);
        assertFalse(playlist.getMediaItems().contains(song1));
    }

    /**
     * Verifie que plusieurs chansons peuvent etre ajouter
     */
    @Test
    public void testAjouterPlusieursChanson(){
        List<MediaItem> lstSongs = new ArrayList<>();
        lstSongs.add(song1);
        lstSongs.add(song2);
        playlist.addAllMedia(lstSongs);
        assertEquals(playlist.getMediaItems(), lstSongs);
    }

    /**
     *Test si le nom de la playliste change
     */
    @Test
    public void testSetPLaylistName(){
        playlist.setPlaylistName("Test");
        assertEquals(playlist.getPlaylistName(), "Test");
    }

    @Test
    public void testCreationPlalist(){
        List<MediaItem> lstSongs = new ArrayList<>();
        lstSongs.add(song1);
        lstSongs.add(song2);
        Playlist test = new Playlist("A List", lstSongs);
        assertEquals(test.getPlaylistName(), "A List");
        assertEquals(test.getMediaItems(),lstSongs);
    }

    @Test
    public void testMoveAfter(){
        addAllSongsToPlaylist();
        playlist.moveAfter(song1, song2);
        assertEquals(song2, playlist.mediaAt(0));
        assertEquals(song1, playlist.mediaAt(1));
    }

    @Test
    public void testMoveBefore(){
        addAllSongsToPlaylist();
        playlist.moveBefore(song2, song1);
        assertEquals(song2, playlist.mediaAt(0));
        assertEquals(song1, playlist.mediaAt(1));
    }

    /**
     * Tests that a save playlist is reloaded with the same title and songs
     */
    @Test
    public void testSaveLoad(){
        addAllSongsToPlaylist();
        try {
            File save = tempFolder.newFile("test.lion");
            playlist.save(save);
            Playlist newPlaylist = new Playlist(save);
            assertEquals(newPlaylist.getPlaylistName(),playlist.getPlaylistName());
            assertEquals(newPlaylist.getMediaItems().size(), playlist.getMediaItems().size());
            for(int i = 0; i < newPlaylist.getMediaItems().size();i++){
                assertEquals(newPlaylist.getMediaItems().get(i).getFile().getAbsolutePath(),playlist.getMediaItems().get(i).getFile().getAbsolutePath());
            }
        } catch (IOException e) {
            e.printStackTrace();
            fail();
        }
    }

    private void addAllSongsToPlaylist(){
        playlist.addMedia(song1);
        playlist.addMedia(song2);
    }
}
