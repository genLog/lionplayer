package model;

import common.*;
import interfaces.Observation;
import javafx.embed.swing.JFXPanel;
import model.exceptions.SongException;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.fail;

/**
 * Created by mbiel on 9/16/2015.
 */
public class PlayerTest {
    private final static int TIMEOUT_MS = 10000; // Temps d'attente pour les changement d'état, augmenter sur ordi lent

    Player player;
    Song song1;
    Song song2;
    Playlist playlist;
    Playlist playlist2;

    @Before
    public void setUp() throws SongException {
        new JFXPanel(); // Pour éviter l'erreur « Toolkit not initialized »

        Preference.clear();

        player = Player.getInstance();
        player.reset();

        song1 = new Song(Common.PATH_TEST_URL1);
        song2 = new Song(Common.PATH_TEST_URL2);
        playlist = new Playlist("Test");
        playlist.addMedia(song1);
        playlist.addMedia(song2);
        playlist2 = new Playlist("Test 2");
        playlist2.addMedia(song2);
        playlist2.addMedia(song1);
        player.setPlaylist(playlist);
    }

    /**
     * Deletes all preferences after running
     */
    @AfterClass
    public static void tearDownClass() {
        Preference.clear();
    }

    @Test
    public void testJouer() throws Exception {
        jouer();
    }

    @Test
    public void testPause() throws Exception {
        jouer();
        attendreObservation(Observation.PLAYER_PAUSED, player::togglePlay);
    }

    @Test
    public void testStop() throws Exception {
        jouer();
        attendreObservation(Observation.PLAYER_STOPPED, player::stop);
    }

    @Test
    public void testChangeMusic() throws Exception {
        jouer();
        Song oldSong = getPlayingSong();
        attendreObservation(Observation.PLAYER_READY, ()->player.setPlaylist(playlist2));
        jouer();
        Song newSong = getPlayingSong();
        assertNotEquals(oldSong, newSong);
    }

    @Test
    public void testSetSingleSongPlaylist() throws Exception {
        player.setSingleSongPlaylist(song1);

        assertEquals(1, player.getPlaylist().getPlaylistLength());
        assertEquals(song1, player.getPlaylist().mediaAt(0));
    }

    @Test
    public void testNextNoRepeatFirstMusicPlaying() throws Exception {
        player.setRepeatState(RepeatState.NO_REPEAT);
        jouer();
        Song oldSong = getPlayingSong();
        attendreObservation(Observation.PLAYER_STARTED, player::next);
        Song currentSong = getPlayingSong();
        assertNotEquals(oldSong, currentSong);
    }

    @Test
    public void testNextNoReapeatLastMusicPLaying() throws Exception {
        player.setRepeatState(RepeatState.NO_REPEAT);
        jouer();
        Song oldSong = getPlayingSong();
        attendreObservation(Observation.PLAYER_STARTED, () -> player.seekPosition(0.99));
        Song currentSong = getPlayingSong();
        assertNotEquals(oldSong, currentSong);
    }


    @Test
    public void testNextRepeatAllLastMusicPlaying(){
        player.setRepeatState(RepeatState.REPEAT_ALL);
        attendreObservation(Observation.PLAYER_STARTED, () -> player.setPlayingSongId(1));
        Song secondSong = getPlayingSong();
        attendreObservation(Observation.PLAYER_STARTED, player::next);
        Song firstSong = getPlayingSong();
        assertNotEquals(secondSong, firstSong);
    }

    @Test
    public void testRepeatOne(){
        player.setRepeatState(RepeatState.REPEAT_ONE);
        jouer();
        Song oldSong = getPlayingSong();
        attendreObservation(Observation.PLAYER_STARTED, () -> player.seekPosition(0.99));
        Song currentSong = getPlayingSong();
        assertEquals(oldSong, currentSong);
    }

    @Test
    public void testRandom(){
        int nbTries = 0;

        player.turnOnShuffle();
        player.setRepeatState(RepeatState.REPEAT_ALL);
        jouer();

        Song phase1FirstSong = getPlayingSong();

        do {
            attendreObservation(Observation.PLAYER_STARTED, player::next);
            attendreObservation(Observation.PLAYER_STARTED, player::next);
            nbTries++;
        } while(phase1FirstSong.equals(getPlayingSong()) && nbTries < 30);

        if(nbTries == 30)
            fail("La lecture aléatoire ne fonctionne pas.");
    }

    @Test
    public void testDeplacerCurseur() throws Exception {
        jouer();
        double seekTo = 0.5;
        player.seekPosition(seekTo);
        assertEquals(true,player.getCurrentSongPosition() <= seekTo );
    }

    /**
     * S'assure que le volume VOLUME_MAX puisse être utilisé
     */
    @Test
    public void testVolumeMax(){
        jouer();
        double volume = Common.VOLUME_MAX;
        player.setVolume(volume);
        assertEquals(volume, player.getVolume(), 0.001);
    }

    /**
     * S'assure que le volume VOLUME_MIN puisse être utilisé
     */
    @Test
    public void testVolumeMin(){
        jouer();
        double volume = Common.VOLUME_MIN;
        player.setVolume(volume);
        assertEquals(volume, player.getVolume(), 0.001);
    }

    /**
     * S'assure que tout volume dépassant VOLUME_MAX ne puisse être utilisé
     */
    @Test
    public void testVolumeOverMax(){
        jouer();
        double volumeBefore = player.getVolume();
        double volume = Common.VOLUME_MAX+0.1;
        player.setVolume(volume);
        assertEquals(volumeBefore, player.getVolume(), 0);
    }

    /**
     * S'assure que tout volume sous VOLUME_MIN ne puisse être utilisé
     */
    @Test
    public void testVolumeUnderMin(){
        jouer();
        double volumeBefore = player.getVolume();
        double volume = Common.VOLUME_MIN-0.1;
        player.setVolume(volume);
        assertEquals(volumeBefore, player.getVolume(), 0);
    }

    private Song getPlayingSong() {
        return (Song) getPlayerFieldValue("currentMedia");
    }


    /**
     * Retourne la valeur d'un champ du Player selon son nom.
     *
     * @param name Nom du champ.
     * @return Objet contenu dans ce champ.
     */
    private Object getPlayerFieldValue(String name) {
        try {
            Field field = Player.class.getDeclaredField(name);
            field.setAccessible(true);
            return field.get(player);
        } catch (NoSuchFieldException | IllegalAccessException ex) {
            ex.printStackTrace();
            fail("Impossible d'accéder au champ Player." + name);
            return null;
        }
    }

    public void jouer(){
        attendreObservation(Observation.PLAYER_STARTED, player::togglePlay);
    }


    public void attendreObservation(Observation etatAttendu, Runnable action) {
        // Observateur sur un thread qui attend un appel avec l'argument spécifié et qui quitte quand c'est le cas
        MockObserver observer = new MockObserver(etatAttendu);
        Thread thread = new Thread(observer);
        thread.setDaemon(true); // Pour eviter les zombie
        thread.start();

        player.addObserver(observer);

        action.run();

        try {
            thread.join(TIMEOUT_MS);
        } catch (InterruptedException e) {
            fail();
        }

        assertEquals(false, thread.isAlive());
    }
}
